import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from 'src/environments/environment.prod';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './components/shared/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/shared/login/login.component';
import { DashboardAdminComponent } from './components/admin/dashboard-admin/dashboard-admin.component';
import { RegisterComponent } from './components/user/register/register.component';
import { FormsModule } from '@angular/forms';
import { CompleteregisterComponent } from './components/user/completeregister/completeregister.component';
import { AlertComponent } from './components/shared/alert/alert.component';
import { ListAreasComponent } from './components/areas/list-areas/list-areas.component';
import { FormAreasComponent } from './components/areas/form-areas/form-areas.component';
import { ListUserComponent } from './components/user/list-user/list-user.component';
import { TypeIdentityPipe } from './pipes/type-identity.pipe';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TestUserComponent } from './components/test-user/test-user.component';
import { TestListComponent } from './components/test-list/test-list.component';
import { TestListTeacherComponent } from './components/test-list-teacher/test-list-teacher.component';
import { TestEvaluateTeacherComponent } from './components/test-evaluate-teacher/test-evaluate-teacher.component';
import { TypeUserPipe } from './pipes/type-user.pipe';
import { MenuMobileComponent } from './components/menu-mobile/menu-mobile.component';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { FilterStatusTestUserPipe } from './pipes/filter-status-test-user.pipe'
import { NgxPaginationModule } from 'ngx-pagination';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    LoginComponent,
    DashboardAdminComponent,
    RegisterComponent,
    CompleteregisterComponent,
    AlertComponent,
    ListAreasComponent,
    FormAreasComponent,
    ListUserComponent,
    TypeIdentityPipe,
    TestUserComponent,
    TestListComponent,
    TestListTeacherComponent,
    TestEvaluateTeacherComponent,
    TypeUserPipe,
    MenuMobileComponent,
    FilterStatusTestUserPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    HttpClientModule,
    FormsModule,
    CKEditorModule,
    NgxMaskModule.forRoot(options),
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }