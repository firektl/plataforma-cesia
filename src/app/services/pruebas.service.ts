import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PruebasService {

  constructor(
    private afs: AngularFirestore
  ) {
    //this.headers = new HttpHeaders( { Authorization: localStorage.getItem('token')  } ) 
  }

  list() {
    let areasRef: AngularFirestoreCollection = this.afs.collection("test");
    return areasRef.valueChanges();
  }

  get(testId: string) {
    let areasRef = this.afs.collection("test").doc(testId);
    return areasRef.get();
  }

  getBy(field: string, value: string) {
    const userRef = this.afs.collection('test', ref => ref.where(field, '==', value))
    return userRef.get();
  }

  getHistory(userId: string) {
    let areasRef = this.afs.collection("test", test => test.where('id_user', '==', userId));
    return areasRef.valueChanges();
  }

  getTestsPendingEvaluate() {
    let areasRef = this.afs.collection("test", test => test.where('status', '==', 1));
    return areasRef.valueChanges();
  }

  getTestsHistoryEvaluate() {
    let areasRef = this.afs.collection("test", test => test.where('status', '==', 2));
    return areasRef.valueChanges();
  }

  store(data) {
    const testCollection = this.afs.collection('test');
    return testCollection.doc(data.id).set(data)
  }

  update(data) {
    /* let areasRef: AngularFirestoreCollection = this.afs.collection('areas', ref => ref.where('id', '==', data.id) );
     areasRef.set(data, { merge: true }) 
     return areasRef.valueChanges()*/
  }


}
