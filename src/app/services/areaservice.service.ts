import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { Areas } from "../interfaces/areas";
import { BanksService } from './banks.service';

@Injectable({
  providedIn: "root"
})
export class AreaserviceService {
  constructor(
    private afs: AngularFirestore,
    private banksservice: BanksService
  ) { }

  list() {
    let areasRef: AngularFirestoreCollection = this.afs.collection("areas");
    return areasRef.valueChanges();
  }

  get(areaId: string) {
    console.log(areaId)
    let areasRef = this.afs.collection("areas").doc(areaId);
    return areasRef.get();
  } 

  store(data: Areas) {   
    data.test.forEach(test => {
      if(test.id_bank === undefined || test.id_bank === ''){
        const id_bank = this.afs.createId();
        test.id_bank = id_bank
        this.banksservice.store({ id: id_bank, questions: [] })
      } 

      if(test.id === undefined || test.id === ''){
        test.id = this.afs.createId()
      }
    });

    const areasCollection = this.afs.collection("areas");
    const id = this.afs.createId();
    data.id = id; 
    return areasCollection.doc(id).set(data);
  }

  update(data: Areas) {
    data.test.forEach(test => {
      if(test.id_bank === undefined || test.id_bank === ''){
        const id_bank = this.afs.createId();
        test.id_bank = id_bank
        this.banksservice.store({ id: id_bank, questions: [] })
      } 

      if(test.id === undefined || test.id === ''){
        test.id = this.afs.createId()
      }
    });

    return this.afs
      .collection("areas")
      .doc(data.id)
      .set(data);
  }

  delete(areaId: string) {
    let areasRef = this.afs.collection("areas").doc(areaId);
    return areasRef.delete();
  } 
}
