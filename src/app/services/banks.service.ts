import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection
} from "@angular/fire/firestore";

@Injectable({
  providedIn: 'root'
})
export class BanksService {

  constructor(
    private afs: AngularFirestore
  ) { }

  store(data){
    const areasCollection = this.afs.collection("bancos"); 
    return areasCollection.doc(data.id).set(data);
  }

  get(bankId: string) {
    let bankRef = this.afs.collection("bancos").doc(bankId)
    return bankRef.get();
  }

  update(data) {
    let bankRef = this.afs.collection("bancos").doc(data.id)
    return bankRef.set(data);
  }

  delete(id: string){
    let bankRef = this.afs.collection("bancos").doc(id).delete()
  }
}

