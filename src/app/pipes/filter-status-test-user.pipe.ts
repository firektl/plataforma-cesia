import { Pipe, PipeTransform } from '@angular/core';
import { Testusuarios } from '../interfaces/testusuarios';

@Pipe({
  name: 'filterStatusTestUser'
})
export class FilterStatusTestUserPipe implements PipeTransform {

  transform(items: Testusuarios[], params: any) {
    if (!items || !params) {
      return items;
    }

    return items.filter(obj => {
      console.log(obj.status, params)
      return Object.keys(params).every(propertyName => obj.status === params[propertyName]);
    });

  }

}
