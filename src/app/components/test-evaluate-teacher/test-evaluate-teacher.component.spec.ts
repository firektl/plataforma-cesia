import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestEvaluateTeacherComponent } from './test-evaluate-teacher.component';

describe('TestEvaluateTeacherComponent', () => {
  let component: TestEvaluateTeacherComponent;
  let fixture: ComponentFixture<TestEvaluateTeacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestEvaluateTeacherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestEvaluateTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
