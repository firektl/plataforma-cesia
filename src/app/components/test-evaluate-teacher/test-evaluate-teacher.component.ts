import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PruebasService } from 'src/app/services/pruebas.service';
import { Testusuarios } from 'src/app/interfaces/testusuarios';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { GeneralserviceService } from 'src/app/services/generalservice.service';
import { UsersService } from 'src/app/services/users.service';
import { Usuarios } from 'src/app/interfaces/usuarios';
import { Areas } from 'src/app/interfaces/areas';
import { Subscription } from 'rxjs';
import { AreaserviceService } from 'src/app/services/areaservice.service';


@Component({
  selector: 'app-test-evaluate-teacher',
  templateUrl: './test-evaluate-teacher.component.html',
  styleUrls: ['./test-evaluate-teacher.component.scss']
})
export class TestEvaluateTeacherComponent implements OnInit {
  public Editor = ClassicEditor
  public configEditor = { language: 'es', isReadOnly: false }
  public inLoad: boolean = true
  public resultSaveTest = {
    msg: '',
    saved: false,
    inSave: false
  }

  public modalConfirmEvaluate: boolean = false
  public areasData: Areas[] = []

  public userTestData: Usuarios = {
    identity: '',
    identityType: '',
    email: '',
    firstName: '',
    lastName: '',
    phone: '',
    areas: []
  }

  public pruebaData: Testusuarios = {
    name: 'Prueba',
    id_test: '',
    id_area: '',
    id_user: '',
    questions: [],
    score: 0,
    comments: '',
    status: 0,
    dateRealize: null,
    dateEvaluate: null
  }

  constructor(
    private route: ActivatedRoute,
    private pruebasservice: PruebasService,
    private generalservice: GeneralserviceService,
    private areasservice: AreaserviceService,
    private usersservice: UsersService,
    private router: Router

  ) { }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get("id") !== null) {
      this.pruebaData.id = this.route.snapshot.paramMap.get("id");
      this.getAreasData();
    }
  }

  loadData() {
    this.inLoad = true
    this.pruebasservice.get(this.pruebaData.id).subscribe(
      data => {
        this.pruebaData = data.data() as Testusuarios
        this.getDataUserTest()
        if (this.pruebaData.status === 1 || this.pruebaData.status === 2)
          this.configEditor.isReadOnly = true

        this.areasData.forEach(area => {
          if (this.pruebaData.id_area === area.id) {
            this.pruebaData.name_area = area.name
          }
        });
        //console.log(this.pruebaData)
        this.inLoad = false
      }
    )
  }

  getAreasData() {
    let subscripcion: Subscription
    subscripcion = this.areasservice.list().subscribe(
      data => {
        this.areasData = data as Areas[]
        this.loadData()
        this.unsubscribeMethod(subscripcion)
      }, error => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error cargando las areas, intente nuevamente o verifique su conexion a internet.",
          type: "error"
        });
      }
    )
  }

  getDataUserTest() {
    this.usersservice.get(this.pruebaData.id_user).subscribe(
      data => {
        this.userTestData = data.data() as Usuarios
      }
    )
  }

  modalConfirmEvaluateOpen() {
    this.modalConfirmEvaluate = true
    this.resultSaveTest.msg = ''
    this.resultSaveTest.inSave = false
    this.resultSaveTest.saved = false
  }

  saveTestEvaluation() {
    this.resultSaveTest.inSave = true
    this.pruebaData.status = 2
    this.pruebaData.dateEvaluate = new Date()
    this.pruebasservice.store(this.pruebaData).then(
      () => {
        this.resultSaveTest.inSave = false
        this.resultSaveTest.saved = true
        this.generalservice.eventShowAlert.emit({
          text: "Se ha guardado la evaluacion exitosamente",
          type: "success"
        });
        this.router.navigate(['profesor', 'test', 'lista'])
      },
      error => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error guardando la evaluacion, intente nuevamente",
          type: "error"
        });
      }
    )
  }

  unsubscribeMethod(suscripcion: Subscription) {
    suscripcion.unsubscribe()
  }
}

