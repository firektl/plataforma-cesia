import { Component, OnInit } from '@angular/core';
import { AreaserviceService } from 'src/app/services/areaservice.service';
import { Areas } from 'src/app/interfaces/areas';

@Component({
  selector: 'app-list-areas',
  templateUrl: './list-areas.component.html',
  styleUrls: ['./list-areas.component.scss']
})
export class ListAreasComponent implements OnInit {

  public areasData: Areas[]
  constructor(
    private areasservice: AreaserviceService
  ) { }

  ngOnInit() {
    this.load()
  }

  load(){
    this.areasservice.list().subscribe(
      data => {
        this.areasData = data as Areas[]  
      }
    )
  }


}
