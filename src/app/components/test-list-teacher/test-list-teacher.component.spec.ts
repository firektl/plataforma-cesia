import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestListTeacherComponent } from './test-list-teacher.component';

describe('TestListTeacherComponent', () => {
  let component: TestListTeacherComponent;
  let fixture: ComponentFixture<TestListTeacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestListTeacherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestListTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
