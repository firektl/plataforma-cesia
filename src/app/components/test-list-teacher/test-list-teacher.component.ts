import { Component, OnInit } from '@angular/core';
import { PruebasService } from 'src/app/services/pruebas.service';
import { AuthserviceService } from 'src/app/services/authservice.service';
import { AreaserviceService } from 'src/app/services/areaservice.service';
import { Areas } from 'src/app/interfaces/areas';
import { Usuarios } from 'src/app/interfaces/usuarios';
import { Areasusuarios } from 'src/app/interfaces/areasusuarios';
import { Test } from 'src/app/interfaces/test';
import { Testusuarios } from 'src/app/interfaces/testusuarios';
import { BanksService } from 'src/app/services/banks.service';
import { GeneralserviceService } from 'src/app/services/generalservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { PreguntasUsuarios } from 'src/app/interfaces/preguntas-usuarios';
import { Subscription } from 'rxjs';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-test-list-teacher',
  templateUrl: './test-list-teacher.component.html',
  styleUrls: ['./test-list-teacher.component.scss']
})
export class TestListTeacherComponent implements OnInit {

  public tabTestPending: boolean = true
  public tabTestHistory: boolean = false
  public inLoad: boolean = false

  public userData: Usuarios
  public areasTestUser: Areas[] = []
  public testPendingData: Testusuarios[] = []
  public testHistoryData: Testusuarios[] = []
  public areasData: Areas[] = []

  public filters = {
    area: '',
    order: 'old',
    user: '',
  }

  public modalConfirmTestStart: boolean = false
  public areaSelData: Areas = {
    name: '',
    description: '',
    test: []
  }
  public testSelData: Test = {
    name: '',
    description: '',
    maxScore: 0,
    nroQuestions: 0,
    id_bank: ''
  }
  public inGenerateTest: boolean = false

  constructor(
    private authservice: AuthserviceService,
    private areasservice: AreaserviceService,
    private bankservice: BanksService,
    private pruebasservice: PruebasService,
    private generalservice: GeneralserviceService,
    private router: Router,
    private route: ActivatedRoute,
    private usersservice: UsersService,
    private afs: AngularFirestore
  ) { }

  ngOnInit() {
    this.getUserData()
    this.getAreasData()
  }

  getUserData() {
    this.inLoad = true
    this.userData = this.authservice.getUserData();

  }

  getAreasData() {
    let subscripcion: Subscription
    subscripcion = this.areasservice.list().subscribe(
      data => {
        this.areasData = data as Areas[]
        if (this.route.snapshot.paramMap.get("tab") !== null) {
          if (this.route.snapshot.paramMap.get("tab") === 'historial') {
            this.tabTestPending = false
            this.tabTestHistory = true
            this.getTestsDataHistory()
          } else {
            this.getTestsDataPending()
          }
        } else {
          this.getTestsDataPending()
        }
        this.unsubscribeMethod(subscripcion)
      }, error => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error cargando las pruebas, intente nuevamente o verifique su conexion a internet.",
          type: "error"
        });
      }
    )
  }

  getTestsDataPending() {
    let subscripcion: Subscription
    subscripcion = this.pruebasservice.getTestsPendingEvaluate().subscribe(
      data => {
        this.testPendingData = data as Testusuarios[]
        this.testPendingData.forEach((test: Testusuarios, index: number) => {
          this.getDataUserTestP(test.id_user, index)
          this.areasData.forEach(
            area => {
              if (test.id_area == area.id) {
                test.name_area = area.name
              }
            }
          )
        })

        //console.log(this.testPendingData)
        this.unsubscribeMethod(subscripcion)
      },
      error => {
        this.generalservice.eventShowAlert.emit({
          text: "Error obteniendo historial de pruebas pendientes",
          type: "error"
        });
      }
    )
  }

  getDataUserTestP(userid: string, index: number) {
    this.usersservice.get(userid).subscribe(
      data => {
        this.testPendingData[index].userTestData = data.data() as Usuarios
      }
    )
  }

  getDataUserTestH(userid: string, index: number) {
    this.usersservice.get(userid).subscribe(
      data => {
        this.testHistoryData[index].userTestData = data.data() as Usuarios
      }
    )
  }

  confirmTest(area: Areas, test: Test) {
    this.modalConfirmTestStart = true
    this.areaSelData = area
    this.testSelData = test
  }

  saveTestUser(data) {
    let idTest = this.afs.createId()
    data.id = idTest
    this.pruebasservice.store(data).then(
      () => {
        this.generalservice.eventShowAlert.emit({
          text: "Prueba generada exitosamente",
          type: "success"
        });
        this.router.navigate(['test', 'realizar', idTest])
      },
      error => {
        this.generalservice.eventShowAlert.emit({
          text: "Error genrando la prueba",
          type: "error"
        });
      }
    )
  }


  getTestsDataHistory() {
    let subscripcion: Subscription
    subscripcion = this.pruebasservice.getTestsHistoryEvaluate().subscribe(
      data => {
        this.testHistoryData = data as Testusuarios[]
        this.testHistoryData.forEach((test: Testusuarios, index: number) => {
          this.getDataUserTestH(test.id_user, index)
          this.areasData.forEach(
            area => {
              if (test.id_area == area.id) {
                test.name_area = area.name
              }
            }
          )
        })
        //console.log(this.testHistoryData)
        this.unsubscribeMethod(subscripcion)
      },
      error => {
        this.generalservice.eventShowAlert.emit({
          text: "Error obteniendo historial de pruebas",
          type: "error"
        });
      }
    )
  }

  unsubscribeMethod(suscripcion: Subscription) {
    suscripcion.unsubscribe()
  }

}
