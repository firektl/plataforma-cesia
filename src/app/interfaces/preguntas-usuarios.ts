export interface PreguntasUsuarios {
    id?: string,
    type: string,
    text: string,
    answer: string,
    score: number
}
