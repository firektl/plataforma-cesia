import { Preguntas } from './preguntas';

export interface Bancos {
    id? : string,
    questions: Preguntas[]
}
