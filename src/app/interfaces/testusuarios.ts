import { PreguntasUsuarios } from './preguntas-usuarios';
import { Usuarios } from './usuarios';

export interface Testusuarios {
    id?: string,
    name: string,
    id_test: string,
    id_area: string,
    id_user: string,
    questions: PreguntasUsuarios[],
    score: number,
    comments: string
    status: number,
    dateRealize: Date,
    dateEvaluate: Date,
    userTestData?: Usuarios
    name_area?: string
}
