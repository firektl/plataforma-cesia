import { Areasusuarios } from './areasusuarios';

export interface Usuarios {
    id?: string,
    email: string,
    firstName: string,
    lastName: string,
    identity: string,
    identityType: string,
    phone: string,
    photoURL?: string,
    code?: string,
    active?: boolean,
    type?: number,
    areas: Areasusuarios[]
}
