// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  name_company: 'ESCONFA INTERNACIONAL',
  firebase: {
    apiKey: "AIzaSyDlE2Du6Y8OVE4hz8Mk-oNjyJklDugBEhY",
    authDomain: "plataforma-cesia-f9c41.firebaseapp.com",
    databaseURL: "https://plataforma-cesia-f9c41.firebaseio.com",
    projectId: "plataforma-cesia-f9c41",
    storageBucket: "plataforma-cesia-f9c41.appspot.com",
    messagingSenderId: "388478824692",
    appId: "1:388478824692:web:e85632e1a55dbbbcda6ea1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
