(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<router-outlet *ngIf=\"!authservice.isLoggedIn; else home\"></router-outlet>\r\n\r\n<ng-template #home>\r\n  <clr-main-container>\r\n\r\n    <app-alert [options]=\"alert\"></app-alert>\r\n    <app-header></app-header>\r\n    <div class=\"content-container\">\r\n      <main class=\"content-area\">\r\n        <router-outlet></router-outlet>\r\n      </main>\r\n      <app-menu-mobile></app-menu-mobile>\r\n    </div>\r\n\r\n  </clr-main-container>\r\n</ng-template>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/admin/dashboard-admin/dashboard-admin.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/admin/dashboard-admin/dashboard-admin.component.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>dashboard-admin works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/areas/form-areas/form-areas.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/areas/form-areas/form-areas.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1>\r\n  <clr-icon shape=\"administrator\" size=\"20\"></clr-icon> Administracion\r\n  <clr-icon shape=\"play\" size=\"20\"></clr-icon> Curso\r\n</h1>\r\n\r\n<div class=\"content-area bg-ca mx-ca br-ca\">\r\n\r\n  <button class=\"btn btn-primary\" routerLink=\"/admin/areas\" routerLinkActive=\"active\">\r\n    <clr-icon shape=\"list\"></clr-icon>Ir a listado\r\n  </button>\r\n\r\n  <div class=\"clr-row\">\r\n    <div class=\"clr-col-md-6 clr-col-12\">\r\n      <h4> Curso: {{ areaData.name  }}</h4>\r\n      <form clrForm>\r\n        <clr-input-container>\r\n          <label>Nombre</label>\r\n          <input clrInput type=\"text\" [disabled]=\"disableForm\" [(ngModel)]=\"areaData.name\" name=\"name\" required />\r\n          <clr-control-error>Este campo es necesario</clr-control-error>\r\n        </clr-input-container>\r\n        <clr-textarea-container>\r\n          <label>Desc.</label>\r\n          <textarea clrTextarea [disabled]=\"disableForm\" [(ngModel)]=\"areaData.description\"\r\n            name=\"description\"></textarea>\r\n        </clr-textarea-container>\r\n\r\n        <div class=\"clr-form-control clr-row\" *ngIf=\"areaData.id\">\r\n          <label class=\"clr-control-label clr-col-12 clr-col-md-2\">\r\n            Pruebas del curso\r\n          </label>\r\n          <div>\r\n            <button class=\"btn btn-outline\" (click)=\"formNewTest()\" [disabled]=\"disableForm\">\r\n              <clr-icon shape=\"add\"></clr-icon>Agregar Prueba a este curso\r\n            </button>\r\n            <ul class=\"list\">\r\n              <li *ngFor=\"let test of areaData.test; index as i \" (click)=\"loadDetailTest(test, i)\">\r\n                <button href=\"#\" class=\"btn btn-link\" [disabled]=\"disableForm\">\r\n                  <clr-icon shape=\"e-check\"></clr-icon> {{ test.name }}\r\n                </button>\r\n              </li>\r\n            </ul>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"pt-1\">\r\n          <button class=\"btn btn-success\" (click)=\"store()\" [disabled]=\"disableForm\">\r\n            <clr-icon shape=\"floppy\"></clr-icon> Guardar\r\n          </button>\r\n          <button class=\"btn btn-danger\" (click)=\"deletePrueba()\" [disabled]=\"disableForm\" *ngIf=\"areaData.id\">\r\n            <clr-icon shape=\"trash\"></clr-icon> Eliminar\r\n          </button>\r\n        </div>\r\n\r\n      </form>\r\n    </div>\r\n    <div class=\"clr-col-md-6 clr-col-12\">\r\n      <div class=\"bg-white\" *ngIf=\"disableForm\">\r\n        <h4>Prueba: {{ newTest.name }} ( {{ ( newTest.id ) ? 'Editar' : 'Nueva'  }} )</h4>\r\n        <form clrForm>\r\n          <clr-input-container>\r\n            <label>Nombre</label>\r\n            <input clrInput type=\"text\" [disabled]=\"disableFormTest\" [(ngModel)]=\"newTest.name\" name=\"testname\"\r\n              required />\r\n            <clr-control-error>Este campo es necesario</clr-control-error>\r\n          </clr-input-container>\r\n          <clr-textarea-container>\r\n            <label>Desc.</label>\r\n            <textarea clrTextarea [(ngModel)]=\"newTest.description\" [disabled]=\"disableFormTest\"\r\n              name=\"testdescription\"></textarea>\r\n          </clr-textarea-container>\r\n          <clr-input-container>\r\n            <label>Maxima puntuacion</label>\r\n            <input clrInput type=\"text\" [(ngModel)]=\"newTest.maxScore\" [disabled]=\"disableFormTest\" name=\"testmaxScore\"\r\n              required />\r\n            <clr-control-error>Este campo es necesario</clr-control-error>\r\n          </clr-input-container>\r\n          <clr-input-container>\r\n            <label>Nro de preguntas</label>\r\n            <input clrInput type=\"text\" [(ngModel)]=\"newTest.nroQuestion\" name=\"testnroQuestion\"\r\n              [disabled]=\"disableFormTest\" required />\r\n            <clr-control-error>Este campo es necesario</clr-control-error>\r\n          </clr-input-container>\r\n          <div class=\"pt-1\">\r\n\r\n            <button [disabled]=\"disableFormTest\" class=\"btn btn-success\" (click)=\"storeTest(newTest)\">\r\n              <clr-icon shape=\"add\"></clr-icon> Guardar Prueba\r\n            </button>\r\n            <button [disabled]=\"disableFormTest\" class=\"btn btn-danger\" *ngIf=\"newTest.id\" (click)=\"deleteTest()\">\r\n              <clr-icon shape=\"trash\"></clr-icon> Eliminar\r\n            </button>\r\n            <button [disabled]=\"disableFormTest\" class=\"btn btn-primary\" *ngIf=\"newTest.id\" (click)=\"loadBank()\">\r\n              <clr-icon shape=\"help\"></clr-icon> Banco de preguntas\r\n            </button>\r\n          </div>\r\n\r\n          <div>\r\n            <button [disabled]=\"disableFormTest\" class=\"btn btn-secondary\" (click)=\"cancelTest()\">\r\n              <clr-icon shape=\"times\"></clr-icon> Cerrar\r\n            </button>\r\n          </div>\r\n\r\n        </form>\r\n      </div>\r\n\r\n    </div>\r\n    <div class=\"clr-col-12\" id=\"bankSector\" #bankSector>\r\n      <div class=\"bg-white\" *ngIf=\"inBank\">\r\n        <h4>\r\n          <clr-icon shape=\"help\"></clr-icon> Banco de preguntas\r\n        </h4>\r\n        <h5> Curso {{ areaData.name }} <clr-icon shape=\"play\" size=\"20\"></clr-icon> Prueba: {{ newTest.name }}</h5>\r\n        <div>\r\n          <button class=\"btn btn-success\" (click)=\"addQuestion()\">\r\n            <clr-icon shape=\"plus\"></clr-icon> Agregar Pregunta\r\n          </button>\r\n          <button class=\"btn btn-primary\" (click)=\"saveBank()\">\r\n            <clr-icon shape=\"floppy\"></clr-icon> Guardar\r\n          </button>\r\n          <button class=\"btn btn-secondary\" (click)=\"closeBank()\">\r\n            <clr-icon shape=\"times\"></clr-icon> Cerrar\r\n          </button>\r\n        </div>\r\n\r\n        <div class=\"clr-row\">\r\n          <div class=\"clr-col-md-4 clr-row-12\" *ngFor=\"let question of bankSelect.questions ; index as i \">\r\n            <div class=\"card\">\r\n              <div class=\"card-block\">\r\n                <h4 class=\"card-title\"># {{ i + 1 }} - Tipo: {{ 'abierta' }}</h4>\r\n                <p [innerHTML]=\"question.text\" class=\"card-text\"></p>\r\n              </div>\r\n              <div class=\"card-block\">\r\n                <button class=\"btn btn-success-online\" (click)=\"editQuestion(question, i)\">\r\n                  <clr-icon shape=\"pencil\"></clr-icon> Editar\r\n                </button>\r\n                <button class=\"btn btn-danger-outline\" (click)=\"deleteQuestion(question)\">\r\n                  <clr-icon shape=\"trash\"></clr-icon> Eliminar\r\n                </button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n\r\n<clr-modal [(clrModalOpen)]=\"modalDeletePrueba\">\r\n  <h3 class=\"modal-title\">¿Esta seguro de eliminar esta area?</h3>\r\n  <div class=\"modal-body\">\r\n    <p>Todas las pruebas y preguntas del banco correspondiente seran eliminadas!</p>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-outline\" (click)=\"modalDeletePrueba = false\">\r\n      <clr-icon shape=\"times\"></clr-icon> No, Cancelar\r\n    </button>\r\n    <button type=\"button\" class=\"btn btn-danger\" (click)=\"confirmDeleteArea()\">\r\n      <clr-icon shape=\"trash\"></clr-icon> Si, Eliminar\r\n    </button>\r\n  </div>\r\n</clr-modal>\r\n\r\n<clr-modal [(clrModalOpen)]=\"modalConfirmDelete\">\r\n  <h3 class=\"modal-title\">¿Esta seguro de eliminar esta prueba?</h3>\r\n  <div class=\"modal-body\">\r\n    <p>Todas las preguntas del banco correspondiente seran eliminadas!</p>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-outline\" (click)=\"modalConfirmDelete = false\">\r\n      <clr-icon shape=\"times\"></clr-icon> No, Cancelar\r\n    </button>\r\n    <button type=\"button\" class=\"btn btn-danger\" (click)=\"confirmDeleteTest()\">\r\n      <clr-icon shape=\"trash\"></clr-icon> Si, Eliminar\r\n    </button>\r\n  </div>\r\n</clr-modal>\r\n\r\n<clr-modal [(clrModalOpen)]=\"modalQuestion\" [clrModalSize]=\"'lg'\">\r\n  <h3 class=\"modal-title\">{{ (questionData.id !== '') ? 'Editar pregunta' : 'Agregar Pregunta' }}</h3>\r\n  <div class=\"modal-body\">\r\n    <clr-select-container>\r\n      <label>Tipo:</label>\r\n      <select clrSelect [(ngModel)]=\"questionData.type\" name=\"type\">\r\n        <option value=\"open\">Abierta</option>\r\n      </select>\r\n    </clr-select-container>\r\n    <div class=\"clr-form-control clr-row\">\r\n      <ckeditor [config]=\"configEditor\" [(ngModel)]=\"questionData.text\" [editor]=\"Editor\"></ckeditor>\r\n    </div>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-outline\" (click)=\"cancelAddQuestion()\">\r\n      <clr-icon shape=\"times\"></clr-icon> Cancelar\r\n    </button>\r\n    <button type=\"button\" class=\"btn btn-success\" (click)=\"confirmAddQuestion()\">\r\n      <clr-icon shape=\"floppy\"></clr-icon> Guardar\r\n    </button>\r\n  </div>\r\n</clr-modal>\r\n\r\n<clr-modal [(clrModalOpen)]=\"modalConfirmDeleteQuestion\">\r\n  <h3 class=\"modal-title\">¿Esta seguro de eliminar esta pregunta?</h3>\r\n  <div class=\"modal-body\">\r\n    <p> Tipo: {{ questionData.type }} </p>\r\n    <p> Texto: </p>\r\n    <p [innerHTML]=\"questionData.text\" class=\"card-text\"></p>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-outline\" (click)=\"cancelDeleteQuestion()\">\r\n      <clr-icon shape=\"times\"></clr-icon> No, Cancelar\r\n    </button>\r\n    <button type=\"button\" class=\"btn btn-danger\" (click)=\"confirmDeleteQuestion()\">\r\n      <clr-icon shape=\"trash\"></clr-icon> Si, Eliminar\r\n    </button>\r\n  </div>\r\n</clr-modal>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/areas/list-areas/list-areas.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/areas/list-areas/list-areas.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1>\r\n  <clr-icon shape=\"administrator\" size=\"20\"></clr-icon> Administracion <clr-icon shape=\"play\" size=\"20\"></clr-icon>\r\n  Cursos\r\n</h1>\r\n\r\n<div class=\"content-area bg-ca mx-ca br-ca\">\r\n\r\n  <button class=\"btn btn-success\" routerLink=\"/admin/areas/nuevo\" routerLinkActive=\"active\">\r\n    <clr-icon shape=\"plus\"></clr-icon>Nuevo Curso\r\n  </button>\r\n\r\n  <div class=\"clr-row\">\r\n    <div class=\"clr-col-lg-4 clr-col-sm-6 clr-col-12\" *ngFor=\"let area of areasData\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          {{ area.name }}\r\n        </div>\r\n        <div class=\"card-block\">\r\n          {{ area.description }}\r\n        </div>\r\n        <div class=\"card-block\">\r\n          <div *ngIf=\"area.test.length > 0\">\r\n            <h5><b>Pruebas</b></h5>\r\n            <ul class=\"list\">\r\n              <li *ngFor=\"let test of area.test\">\r\n                {{ test.name }}\r\n              </li>\r\n            </ul>\r\n          </div>\r\n          <div *ngIf=\"area.test.length == 0\">\r\n            <small>No hay pruebas creadas.</small>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-footer\">\r\n          <a class=\"btn btn-primary\" [routerLink]=\" '/admin/areas/editar/' + area.id \"\r\n            routerLinkActive=\"active\">Editar</a>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/dashboard/dashboard.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/dashboard/dashboard.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1 *ngIf=\"userData !== undefined\">Hola {{ userData.firstName }}, {{ textSaludo }} ! </h1>\r\n<div>A continuación te mostramos lo que puedes hacer</div>\r\n\r\n<div class=\"content-area bg-ca mx-ca br-ca\">\r\n\r\n  <div class=\"clr-row\">\r\n    <div class=\"clr-col-md-4 clr-col-12\">\r\n      <div class=\"card\" *ngIf=\"userData.type === 2; else cardPruebasProfesor\">\r\n        <div class=\"card-header\">\r\n          Mis pruebas\r\n        </div>\r\n        <div class=\"card-block\">\r\n          En esta seccion encontraras las pruebas disponibles, y podras consultar tu historial.\r\n        </div>\r\n        <div class=\"card-footer\">\r\n          <a class=\"btn btn-primary\" routerLink=\"/test/usuario\" routerLinkActive=\"active\">\r\n            <clr-icon shape=\"bullet-list\"></clr-icon>\r\n            Ir a mis pruebas\r\n          </a>\r\n\r\n        </div>\r\n      </div>\r\n      <ng-template #cardPruebasProfesor>\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            Sección de pruebas\r\n          </div>\r\n          <div class=\"card-block\">\r\n            En esta seccion podras evaluar las pruebas realizadas, y ver el historial.\r\n          </div>\r\n          <div class=\"card-footer\">\r\n            <a class=\"btn btn-primary\" routerLink=\"/profesor/test/lista\" routerLinkActive=\"active\">\r\n              <clr-icon shape=\"bullet-list\"></clr-icon>\r\n              Ver y evaluar pruebas\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </ng-template>\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/menu-mobile/menu-mobile.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/menu-mobile/menu-mobile.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"sidenav\" [clr-nav-level]=\"1\">\n  <section class=\"sidenav-content\">\n    <a href=\"#\" class=\"nav-link\"><span class=\"nav-text\">Inicio</span></a>\n    <a *ngIf=\"userLoad && userData.type === 2\" routerLink=\"/test/usuario\" routerLinkActive=\"active\"\n      class=\"nav-link\"><span class=\"nav-text\">Mis Pruebas</span></a>\n    <a *ngIf=\"userLoad && userData.type !== 2\" routerLink=\"/profesor/test/lista\" routerLinkActive=\"active\"\n      class=\"nav-link\"><span class=\"nav-text\">Pruebas</span></a>\n    <a *ngIf=\"userLoad && userData.type !== 2\" routerLink=\"/admin/areas\" routerLinkActive=\"active\"\n      class=\"nav-link\"><span class=\"nav-text\">Cursos</span></a>\n    <a *ngIf=\"userLoad && userData.type !== 2\" routerLink=\"/admin/usuarios\" routerLinkActive=\"active\"\n      class=\"nav-link\"><span class=\"nav-text\">Usuarios</span></a>\n    <a routerLink=\"/usuario/perfil\" routerLinkActive=\"active\" class=\"nav-link\"><span class=\"nav-text\">\n        <clr-icon shape=\"user\"></clr-icon> Mi perfil\n      </span></a>\n    <a (click)=\"authservice.LogOut()\" class=\"nav-link\"><span class=\"nav-text\">\n        <clr-icon shape=\"logout\"></clr-icon> Cerrar sesion\n      </span></a>\n  </section>\n</nav>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/alert/alert.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/alert/alert.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<clr-alert [clrAlertIcon]=\"options.icon\" [clrAlertType]=\"options.type\" [clrAlertClosable]=\"false\" *ngIf=\"options.show === true\">\r\n    <clr-alert-item>\r\n            <span class=\"alert-text\">\r\n                {{ options.text }}\r\n            </span>\r\n            <!-- <div class=\"alert-actions\">\r\n                <clr-dropdown>\r\n                    <button class=\"dropdown-toggle\" clrDropdownTrigger>\r\n                        Actions\r\n                        <clr-icon shape=\"caret down\"></clr-icon>\r\n                    </button>\r\n                    <clr-dropdown-menu clrPosition=\"bottom-right\">\r\n                        <a href=\"...\" class=\"dropdown-item\" clrDropdownItem>Shutdown</a>\r\n                        <a href=\"...\" class=\"dropdown-item\" clrDropdownItem>Delete</a>\r\n                        <a href=\"...\" class=\"dropdown-item\" clrDropdownItem>Reboot</a>\r\n                    </clr-dropdown-menu>\r\n                </clr-dropdown>\r\n            </div> -->\r\n    </clr-alert-item>\r\n</clr-alert>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/header/header.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/header/header.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<clr-header class=\"header-5\">\r\n  <div class=\"branding\">\r\n    <a href=\"...\" class=\"nav-link\">\r\n      <img src=\"https://i.ibb.co/BsBkh3x/esconfa-mini.png\" alt=\"ESCONFA\" width=\"64\" />\r\n      <span class=\"title\">{{ name_company }}</span>\r\n    </a>\r\n  </div>\r\n  <div class=\"header-nav\">\r\n    <a href=\"#\" class=\"nav-link\"><span class=\"nav-text\">Inicio</span></a>\r\n    <a *ngIf=\"userLoad && userData.type === 2\" routerLink=\"/test/usuario\" routerLinkActive=\"active\"\r\n      class=\"nav-link\"><span class=\"nav-text\">Mis Pruebas</span></a>\r\n    <a *ngIf=\"userLoad && userData.type !== 2\" routerLink=\"/profesor/test/lista\" routerLinkActive=\"active\"\r\n      class=\"nav-link\"><span class=\"nav-text\">Pruebas</span></a>\r\n    <a *ngIf=\"userLoad && userData.type !== 2\" routerLink=\"/admin/areas\" routerLinkActive=\"active\"\r\n      class=\"nav-link\"><span class=\"nav-text\">Cursos</span></a>\r\n    <a *ngIf=\"userLoad && userData.type !== 2\" routerLink=\"/admin/usuarios\" routerLinkActive=\"active\"\r\n      class=\"nav-link\"><span class=\"nav-text\">Usuarios</span></a>\r\n  </div>\r\n  <div class=\"header-actions\">\r\n    <clr-dropdown>\r\n      <button class=\"nav-text\" clrDropdownTrigger aria-label=\"toggle settings menu\">\r\n        <clr-icon shape=\"user\"></clr-icon>\r\n        <span class=\"hideMobile\"> {{  userData.firstName }} </span>\r\n        <clr-icon shape=\"caret down\"></clr-icon>\r\n      </button>\r\n      <clr-dropdown-menu *clrIfOpen clrPosition=\"bottom-right\">\r\n        <a href=\"...\" clrDropdownItem>Mi cuenta</a>\r\n        <a href=\"...\" clrDropdownItem>Contacto</a>\r\n        <a href=\"#\" clrDropdownItem (click)=\"authservice.LogOut()\">Salir</a>\r\n      </clr-dropdown-menu>\r\n    </clr-dropdown>\r\n  </div>\r\n</clr-header>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/login/login.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/login/login.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"login-wrapper\" [style.background]=\"'url(' + urlImg + ')'\">\r\n  <small class=\"infoImgDay\">{{ infoImg }}</small>\r\n  <div class=\"login\">\r\n    <section class=\"title\" style=\"text-align: center;\">\r\n      <img src=\"https://i.ibb.co/BsBkh3x/esconfa-mini.png\" alt=\"ESCONFA\" width=\"200\" />\r\n      <h3 class=\"welcome\">Bienvenid@,</h3>\r\n      {{ name_company }}\r\n      <h5 class=\"hint\">Use su correo electronico y contraseña para acceder</h5>\r\n      <h4 class=\"errorLogin\" *ngIf=\"showError\"> Error, verifique sus datos de acceso! </h4>\r\n      <h4 class=\"successLogin\" *ngIf=\"showError\"> Bienvenido... </h4>\r\n    </section>\r\n    <div class=\"login-group\">\r\n      <clr-input-container>\r\n        <label class=\"clr-sr-only\">Correo</label>\r\n        <input type=\"text\" name=\"email\" clrInput placeholder=\"Correo\" #userName />\r\n      </clr-input-container>\r\n      <clr-password-container>\r\n        <label class=\"clr-sr-only\">Contraseña</label>\r\n        <input type=\"password\" name=\"password\" clrPassword placeholder=\"Contraseña\" #userPassword />\r\n      </clr-password-container>\r\n      <clr-checkbox-wrapper>\r\n        <label>Recordar datos</label>\r\n        <input type=\"checkbox\" name=\"rememberMe\" clrCheckbox #checkRemember />\r\n      </clr-checkbox-wrapper>\r\n\r\n      <button class=\"btn btn-primary\" (click)=\"logIn(userName.value, userPassword.value)\">\r\n        <clr-icon shape=\"login\"></clr-icon> Acceder\r\n      </button>\r\n      <button class=\"btn btn-primary\" class=\"btn btn-success\" routerLink=\"/registro\" routerLinkActive=\"active\">\r\n        <clr-icon shape=\"id-badge\"></clr-icon> Registrarse\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-evaluate-teacher/test-evaluate-teacher.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-evaluate-teacher/test-evaluate-teacher.component.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1><clr-icon shape=\"bullet-list\" size=\"20\"></clr-icon> Prueba: {{  pruebaData.name }} </h1> \r\n<small> Numero de preguntas: {{ pruebaData.questions.length }} </small>\r\n<div class=\"content-area bg-ca mx-ca br-ca\"> \r\n        <div *ngIf=\"inLoad\">\r\n                Cargando...\r\n                <div class=\"progress loop\"><progress></progress></div>\r\n        </div> \r\n        <div class=\"content-container\" *ngIf=\"pruebaData.status === 1 || pruebaData.status === 2\">\r\n                <div class=\"card\"> \r\n                        <div class=\"card-header\">\r\n                                <h3>Información detallada</h3>\r\n                        </div>\r\n                        <div class=\"card-text p-1 mb-0\">\r\n                                <h3> <clr-icon size=\"20\" shape=\"assign-user\"></clr-icon>  <b>Usuario:</b> {{ userTestData.firstName }} {{ userTestData.lastName }} </h3>\r\n                                <h4> <clr-icon size=\"20\" shape=\"envelope\"></clr-icon> {{ userTestData.email }} </h4>\r\n                                <div class=\"pt-1\">\r\n                                    <span class=\"label\"><clr-icon shape=\"calendar\"></clr-icon>  Realizada: {{ pruebaData.dateRealize.seconds * 1000 | date:'dd-MM-yyyy h:mm:ss a' }}</span>\r\n                                    <span class=\"label\" *ngIf=\"pruebaData.status == 2\"><clr-icon shape=\"calendar\"></clr-icon>  Evaluada: {{ pruebaData.dateEvaluate.seconds * 1000 | date:'dd-MM-yyyy h:mm:ss a' }}</span>  \r\n                                </div> \r\n                        </div>\r\n                        <div class=\"card-block\">   \r\n                                <a class=\"btn btn-outline\" *ngIf=\"pruebaData.status === 1\">\r\n                                        <clr-icon shape=\"clock\"></clr-icon>\r\n                                        Estatus: Pendiente por revision\r\n                                </a> \r\n                                <a class=\"btn btn-outline-success\" *ngIf=\"pruebaData.status === 2\">\r\n                                        <clr-icon shape=\"check\"></clr-icon>\r\n                                        Estatus: Evaluado\r\n                                </a>   \r\n                        </div>\r\n                        <div class=\"card-block\" *ngIf=\"pruebaData.status === 2\">\r\n                                <a class=\"btn btn-success\">\r\n                                        <clr-icon shape=\"check\"></clr-icon>\r\n                                        Nota: {{ pruebaData.score }}\r\n                                </a>  \r\n                                <div>\r\n                                        Comentarios de la evaluación\r\n                                </div>\r\n                                <p [innerHTML]=\"pruebaData.comments\"> </p>\r\n                        </div>\r\n                </div>\r\n        </div>\r\n        <div class=\"content-container\" *ngIf=\"!inLoad\">\r\n                <div class=\"clr-row\"> \r\n                        <div class=\"clr-col-12\" *ngFor=\"let question of pruebaData.questions; index as i\">\r\n                                <div class=\"card\">\r\n                                        <div class=\"card-header\">\r\n                                                <div [innerHTML]=\" ( i + 1 ) + ') ' + question.text\"></div>\r\n                                        </div>\r\n                                        <div class=\"card-block\" *ngIf=\"pruebaData.status == 0\">\r\n                                                <ckeditor [config]=\"configEditor\" [(ngModel)]=\"question.answer\" [editor]=\"Editor\"></ckeditor>  \r\n                                        </div> \r\n                                        <div class=\"card-block\"  *ngIf=\"pruebaData.status === 1 || pruebaData.status === 2\">\r\n                                             <p [innerHTML]=\"question.answer\"></p> \r\n                                        </div> \r\n                                </div>\r\n                        </div>  \r\n                </div> \r\n        </div>\r\n        <div *ngIf=\"!inLoad && pruebaData.status == 1\">\r\n            <div class=\"content-container w-100 clr-justify-content-center pt-1\">\r\n              \r\n                    <clr-select-container>\r\n                        <label><h3>Puntaje</h3></label>\r\n                        <select clrSelect [(ngModel)]=\"pruebaData.score\" name=\"score\">\r\n                            <option *ngFor=\"let number of [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]\" [value]=\"number\">{{number}}</option>\r\n                        </select> \r\n                    </clr-select-container>  \r\n              \r\n            </div>\r\n            <div class=\"content-container w-100 clr-justify-content-center pt-1\"> \r\n                    <div>\r\n                        <button class=\"btn btn-success\" (click)=\"modalConfirmEvaluateOpen()\"><clr-icon shape=\"success-standard\"></clr-icon> Guardar Evaluación</button>\r\n                    </div> \r\n            </div>\r\n        </div> \r\n</div>\r\n\r\n<clr-modal [(clrModalOpen)]=\"modalConfirmEvaluate\">\r\n        <h3 class=\"modal-title\">¿Estas seguro(a) de definir la evaluación para la prueba?</h3>\r\n        <div class=\"modal-body\">\r\n                <div *ngIf=\"!resultSaveTest.saved\">\r\n                    <div class=\"card-block\" >\r\n                        <div>\r\n                            <h3>Puntaje asignado: <b> {{ pruebaData.score }}</b></h3>\r\n                        </div>\r\n                        <div class=\"pt-1\">\r\n                            Comentario sobre la evaluación\r\n                            <ckeditor [config]=\" { language: 'es' } \" [(ngModel)]=\"pruebaData.comments\" [editor]=\"Editor\"></ckeditor>  \r\n                        </div> \r\n                    </div> \r\n                </div>\r\n                <div *ngIf=\"resultSaveTest.inSave\">\r\n                        Guardando...\r\n                        <div class=\"progress loop\"><progress></progress></div>\r\n                </div>\r\n                <div *ngIf=\"resultSaveTest.saved\">\r\n                        <h4>Se ha guardado la evaluacion correctamente!</h4>\r\n                </div> \r\n        </div> \r\n        <div class=\"modal-footer\" [hidden]=\"inGenerateTest\">\r\n                <button type=\"button\" class=\"btn btn-outline\" (click)=\"modalConfirmEvaluate = false\">\r\n                        <clr-icon shape=\"times\"></clr-icon> No, Cancelar\r\n                </button>\r\n                <button type=\"button\" [hidden]=\"resultSaveTest.inSave && !resultSaveTest.saved\" class=\"btn btn-success\" (click)=\"saveTestEvaluation()\" >\r\n                        <clr-icon shape=\"success-standard\"></clr-icon> Si, Guardar\r\n                </button>\r\n        </div>\r\n</clr-modal>\r\n            \r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-list-teacher/test-list-teacher.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-list-teacher/test-list-teacher.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1>\r\n  <clr-icon shape=\"help\" size=\"20\"></clr-icon> Pruebas de los usuarios\r\n</h1>\r\n\r\n<div class=\"content-area bg-ca mx-ca br-ca\">\r\n  <clr-tabs>\r\n    <clr-tab>\r\n      <button clrTabLink (click)=\"getTestsDataPending()\">Pruebas pendientes por evaluar</button>\r\n      <ng-template [(clrIfActive)]=\"tabTestPending\">\r\n        <clr-tab-content>\r\n          <div class=\"clr-row w-100\">\r\n            <div class=\"clr-col-12\" *ngFor=\"let test of testPendingData\">\r\n              <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                  {{ test.name }} <small> - {{ test.questions.length }} Preguntas </small>\r\n                </div>\r\n                <div class=\"card-text p-1 mb-0\">\r\n                  <h3>\r\n                    <clr-icon size=\"20\" shape=\"assign-user\"></clr-icon> <b>Usuario:</b>\r\n                    {{ test.userTestData.firstName }} {{ test.userTestData.lastName }}\r\n                  </h3>\r\n                  <h4>\r\n                    <clr-icon size=\"20\" shape=\"envelope\"></clr-icon> {{ test.userTestData.email }}\r\n                  </h4>\r\n                  <div class=\"pt-1\">\r\n                    <span class=\"label\">\r\n                      <clr-icon shape=\"calendar\"></clr-icon> Realizada:\r\n                      {{ test.dateRealize.seconds * 1000 | date:'dd-MM-yyyy h:mm:ss a' }}\r\n                    </span>\r\n                    <span class=\"label\" *ngIf=\"test.status == 2\">\r\n                      <clr-icon shape=\"calendar\"></clr-icon> Evaluada:\r\n                      {{ test.dateEvaluate.seconds * 1000 | date:'dd-MM-yyyy h:mm:ss a' }}\r\n                    </span>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"card-block\">\r\n                  <a class=\"btn btn-outline\" *ngIf=\"test.status === 1\">\r\n                    <clr-icon shape=\"clock\"></clr-icon>\r\n                    Estatus: Pendiente por revision\r\n                  </a>\r\n                  <a class=\"btn btn-outline-success\" *ngIf=\"test.status === 2\">\r\n                    <clr-icon shape=\"check\"></clr-icon>\r\n                    Estatus: Evaluado\r\n                  </a>\r\n                </div>\r\n                <div class=\"card-block\">\r\n                  <a class=\"btn btn-success\" [routerLink]=\" '/profesor/test/evaluar/' + test.id\"\r\n                    routerLinkActive=\"active\">\r\n                    <clr-icon shape=\"bullet-list\"></clr-icon>\r\n                    Evaluar\r\n                  </a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </clr-tab-content>\r\n      </ng-template>\r\n    </clr-tab>\r\n\r\n    <clr-tab>\r\n      <button clrTabLink (click)=\"getTestsDataHistory()\">Pruebas evaluadas</button>\r\n      <ng-template [(clrIfActive)]=\"tabTestHistory\">\r\n        <clr-tab-content>\r\n          <div class=\"content-container\">\r\n            <div class=\"clr-row w-100\">\r\n              <div class=\"clr-col-12\" *ngFor=\"let test of testHistoryData\">\r\n                <div class=\"card\">\r\n                  <div class=\"card-header\">\r\n                    {{ test.name }} <small> - {{ test.questions.length }} Preguntas </small>\r\n                  </div>\r\n                  <div class=\"card-text p-1 mb-0\">\r\n                    <h3>\r\n                      <clr-icon size=\"20\" shape=\"assign-user\"></clr-icon> <b>Usuario:</b>\r\n                      {{ test.userTestData.firstName }} {{ test.userTestData.lastName }}\r\n                    </h3>\r\n                    <h4>\r\n                      <clr-icon size=\"20\" shape=\"envelope\"></clr-icon> {{ test.userTestData.email }}\r\n                    </h4>\r\n                    <div class=\"pt-1\">\r\n                      <span class=\"label\">\r\n                        <clr-icon shape=\"calendar\"></clr-icon> Realizada:\r\n                        {{ test.dateRealize.seconds * 1000 | date:'dd-MM-yyyy h:mm:ss a' }}\r\n                      </span>\r\n                      <span class=\"label\" *ngIf=\"test.status == 2\">\r\n                        <clr-icon shape=\"calendar\"></clr-icon> Evaluada:\r\n                        {{ test.dateEvaluate.seconds * 1000 | date:'dd-MM-yyyy h:mm:ss a' }}\r\n                      </span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"card-block\">\r\n                    <a class=\"btn btn-outline\" *ngIf=\"test.status === 1\">\r\n                      <clr-icon shape=\"clock\"></clr-icon>\r\n                      Estatus: Pendiente por revision\r\n                    </a>\r\n                    <a class=\"btn btn-outline-success\" *ngIf=\"test.status === 2\">\r\n                      <clr-icon shape=\"check\"></clr-icon>\r\n                      Estatus: Evaluado\r\n                    </a>\r\n                  </div>\r\n                  <div class=\"card-block\">\r\n                    <a class=\"btn btn-primary\" [routerLink]=\" '/profesor/test/detalle/' + test.id\"\r\n                      routerLinkActive=\"active\">\r\n                      <clr-icon shape=\"bullet-list\"></clr-icon>\r\n                      Ver detalle\r\n                    </a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </clr-tab-content>\r\n      </ng-template>\r\n    </clr-tab>\r\n  </clr-tabs>\r\n</div>\r\n\r\n\r\n<clr-modal [(clrModalOpen)]=\"modalConfirmTestStart\">\r\n  <h3 class=\"modal-title\">¿Esta seguro que desea realizar esta Prueba?</h3>\r\n  <div class=\"modal-body\">\r\n    <h4> Nombre: {{ testSelData.name }}</h4>\r\n    <h5> Curso: {{ areaSelData.name }} </h5>\r\n  </div>\r\n  <div class=\"modal-footer\" *ngIf=\"inGenerateTest\">\r\n    <clr-progress-bar clrValue=\"75\" clrLoop></clr-progress-bar>\r\n  </div>\r\n  <div class=\"modal-footer\" [hidden]=\"inGenerateTest\">\r\n    <button type=\"button\" class=\"btn btn-outline\" (click)=\"modalConfirmTestStart = false\">\r\n      <clr-icon shape=\"times\"></clr-icon> Ahora no\r\n    </button>\r\n    <button type=\"button\" class=\"btn btn-success\" (click)=\"generateTest()\">\r\n      <clr-icon shape=\"view-list\"></clr-icon> Si, Comenzar\r\n    </button>\r\n  </div>\r\n</clr-modal>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-list/test-list.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-list/test-list.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1>\r\n  <clr-icon shape=\"help\" size=\"20\"></clr-icon> Mis pruebas\r\n</h1>\r\n\r\n<div class=\"content-area bg-ca mx-ca br-ca\">\r\n  <clr-tabs>\r\n    <clr-tab>\r\n      <button clrTabLink>Pruebas disponibles</button>\r\n      <ng-template [(clrIfActive)]=\"tabTestPending\">\r\n        <clr-tab-content>\r\n          <div *ngIf=\"areasTestUser.length > 0; else emptyTest\">\r\n            <div class=\"clr-row\" *ngFor=\"let area of areasTestUser\">\r\n              <div class=\"clr-col-mf-4 clr-col-12\" *ngFor=\"let test of area.test\">\r\n                <div class=\"card\">\r\n                  <div class=\"card-header\">\r\n                    {{ test.name }}\r\n                  </div>\r\n                  <div class=\"card-block\">\r\n                    <clr-icon shape=\"e-check\"></clr-icon>\r\n                    Curso: {{ area.name }}\r\n                  </div>\r\n                  <div class=\"card-block\">\r\n                    {{ test.description }}\r\n                  </div>\r\n                  <div class=\"card-footer\">\r\n                    <a class=\"btn btn-primary\" (click)=\"confirmTest(area, test)\">\r\n                      <clr-icon shape=\"bullet-list\"></clr-icon>\r\n                      Realizar\r\n                    </a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n          <ng-template #emptyTest>\r\n            <div *ngIf=\"inLoad\">\r\n              Cargano...\r\n              <clr-progress-bar clrValue=\"75\" clrLoop></clr-progress-bar>.\r\n            </div>\r\n            <div *ngIf=\"!inLoad\">\r\n              Actualmente no tienes pruebas disponibles por realizar.\r\n            </div>\r\n          </ng-template>\r\n        </clr-tab-content>\r\n      </ng-template>\r\n    </clr-tab>\r\n\r\n    <clr-tab>\r\n      <button clrTabLink (click)=\"loadHistory()\">Pruebas realizadas</button>\r\n      <ng-template [(clrIfActive)]=\"tabTestHistory\">\r\n        <clr-tab-content>\r\n          <div class=\"content-container\">\r\n            <div class=\"clr-row w-100\">\r\n              <div class=\"clr-col-12\" *ngFor=\"let test of testHistoryData\">\r\n                <div class=\"card\">\r\n                  <div class=\"card-header\">\r\n                    {{ test.name }} <small> - {{ test.questions.length }} Preguntas </small>\r\n                  </div>\r\n                  <div class=\"card-text p-1 mb-0\">\r\n                    <span class=\"label\">Realizada:\r\n                      {{ test.dateRealize.seconds * 1000 | date:'dd-MM-yyyy h:mm:ss a' }}</span>\r\n                    <span class=\"label\" *ngIf=\"test.status == 2\">Evaluada:\r\n                      {{ test.dateEvaluate.seconds * 1000 | date:'dd-MM-yyyy h:mm:ss a' }}</span>\r\n\r\n                  </div>\r\n                  <div class=\"card-block\">\r\n                    <a class=\"btn btn-outline\" *ngIf=\"test.status === 1\">\r\n                      <clr-icon shape=\"clock\"></clr-icon>\r\n                      Estatus: Pendiente por revision\r\n                    </a>\r\n                    <a class=\"btn btn-outline-success\" *ngIf=\"test.status === 2\">\r\n                      <clr-icon shape=\"check\"></clr-icon>\r\n                      Estatus: Evaluado\r\n                    </a>\r\n                  </div>\r\n                  <div class=\"card-block\">\r\n                    <a class=\"btn btn-primary\" [routerLink]=\" '/test/detalle/' + test.id\" routerLinkActive=\"active\">\r\n                      <clr-icon shape=\"bullet-list\"></clr-icon>\r\n                      Ver detalle\r\n                    </a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </clr-tab-content>\r\n      </ng-template>\r\n    </clr-tab>\r\n  </clr-tabs>\r\n\r\n\r\n</div>\r\n\r\n\r\n<clr-modal [(clrModalOpen)]=\"modalConfirmTestStart\">\r\n  <h3 class=\"modal-title\">¿Esta seguro que desea realizar esta Prueba?</h3>\r\n  <div class=\"modal-body\">\r\n    <h4> Nombre: {{ testSelData.name }}</h4>\r\n    <h5> Curso: {{ areaSelData.name }} </h5>\r\n  </div>\r\n  <div class=\"modal-footer\" *ngIf=\"inGenerateTest\">\r\n    <clr-progress-bar clrValue=\"75\" clrLoop></clr-progress-bar>\r\n  </div>\r\n  <div class=\"modal-footer\" [hidden]=\"inGenerateTest\">\r\n    <button type=\"button\" class=\"btn btn-outline\" (click)=\"modalConfirmTestStart = false\">\r\n      <clr-icon shape=\"times\"></clr-icon> Ahora no\r\n    </button>\r\n    <button type=\"button\" class=\"btn btn-success\" (click)=\"generateTest()\">\r\n      <clr-icon shape=\"view-list\"></clr-icon> Si, Comenzar\r\n    </button>\r\n  </div>\r\n</clr-modal>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-user/test-user.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-user/test-user.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1><clr-icon shape=\"bullet-list\" size=\"20\"></clr-icon> Prueba: {{  pruebaData.name }} </h1> \r\n<small> Numero de preguntas: {{ pruebaData.questions.length }} </small>\r\n<div class=\"content-area bg-ca mx-ca br-ca\"> \r\n        <div *ngIf=\"inLoad\">\r\n                Cargando...\r\n                <div class=\"progress loop\"><progress></progress></div>\r\n        </div> \r\n        <div class=\"content-container\" *ngIf=\"pruebaData.status === 1 || pruebaData.status === 2\">\r\n                <div class=\"card\"> \r\n                        <div class=\"card-header\">\r\n                                <h3>Información detallada</h3>\r\n                        </div>\r\n                        <div class=\"card-text p-1 mb-0\">\r\n                                <span class=\"label\">Realizada: {{ pruebaData.dateRealize.seconds * 1000 | date:'dd-MM-yyyy h:mm:ss a' }}</span>\r\n                                <span class=\"label\" *ngIf=\"pruebaData.status == 2\">Evaluada: {{ pruebaData.dateEvaluate.seconds * 1000 | date:'dd-MM-yyyy h:mm:ss a' }}</span>  \r\n                        </div>\r\n                        <div class=\"card-block\">   \r\n                                <a class=\"btn btn-outline\" *ngIf=\"pruebaData.status === 1\">\r\n                                        <clr-icon shape=\"clock\"></clr-icon>\r\n                                        Estatus: Pendiente por revision\r\n                                </a> \r\n                                <a class=\"btn btn-outline-success\" *ngIf=\"pruebaData.status === 2\">\r\n                                        <clr-icon shape=\"check\"></clr-icon>\r\n                                        Estatus: Evaluado\r\n                                </a>   \r\n                        </div>\r\n                        <div class=\"card-block\" *ngIf=\"pruebaData.status === 2\">\r\n                                <a class=\"btn btn-success\">\r\n                                        <clr-icon shape=\"check\"></clr-icon>\r\n                                        Nota: {{ pruebaData.score }}\r\n                                </a>  \r\n                                <div>\r\n                                        Comentarios de la evaluación\r\n                                </div>\r\n                                <p [innerHTML]=\"pruebaData.comments\"> </p>\r\n                        </div>\r\n                </div>\r\n        </div>\r\n        <div class=\"content-container\" *ngIf=\"!inLoad\">\r\n                <div class=\"clr-row\"> \r\n                        <div class=\"clr-col-12\" *ngFor=\"let question of pruebaData.questions; index as i\">\r\n                                <div class=\"card\">\r\n                                        <div class=\"card-header\">\r\n                                                <div [innerHTML]=\" ( i + 1 ) + ') ' + question.text\"></div>\r\n                                        </div>\r\n                                        <div class=\"card-block\" *ngIf=\"pruebaData.status == 0\">\r\n                                                <ckeditor [config]=\"configEditor\" [(ngModel)]=\"question.answer\" [editor]=\"Editor\"></ckeditor>  \r\n                                        </div> \r\n                                        <div class=\"card-block\"  *ngIf=\"pruebaData.status === 1 || pruebaData.status === 2\">\r\n                                             <p [innerHTML]=\"question.answer\"></p> \r\n                                        </div> \r\n                                </div>\r\n                        </div>  \r\n                </div> \r\n        </div>\r\n        <div class=\"content-container\" *ngIf=\"!inLoad && pruebaData.status == 0\">\r\n                <div class=\"clr-row w-100 clr-justify-content-center pt-1\">\r\n                        <div>\r\n                                <button class=\"btn btn-success\" (click)=\"modalConfirmTest()\"><clr-icon shape=\"check\"></clr-icon> Guardar prueba</button>\r\n                        </div> \r\n                </div>\r\n        </div> \r\n</div>\r\n\r\n<clr-modal [(clrModalOpen)]=\"modalConfirmSaveTest\">\r\n        <h3 class=\"modal-title\">¿Estas seguro de haber terminado la prueba?</h3>\r\n        <div class=\"modal-body\">\r\n                <div *ngIf=\"!resultSaveTest.saved\">\r\n                        Haz click en la opcion de tu preferencia para continuar\r\n                </div>\r\n                <div *ngIf=\"resultSaveTest.inSave\">\r\n                        Guardando...\r\n                        <div class=\"progress loop\"><progress></progress></div>\r\n                </div>\r\n                <div *ngIf=\"resultSaveTest.saved\">\r\n                        <h4>Se ha guardado la prueba correctamente!</h4>\r\n                </div> \r\n        </div>\r\n        <div class=\"modal-footer\" *ngIf=\"inGenerateTest\">\r\n                <clr-progress-bar clrValue=\"75\" clrLoop></clr-progress-bar>\r\n        </div>\r\n        <div class=\"modal-footer\" [hidden]=\"inGenerateTest\">\r\n                <button type=\"button\" class=\"btn btn-outline\" (click)=\"modalConfirmSaveTest = false\">\r\n                        <clr-icon shape=\"times\"></clr-icon> No, Cancelar\r\n                </button>\r\n                <button type=\"button\" [hidden]=\"resultSaveTest.inSave && !resultSaveTest.saved\" class=\"btn btn-success\" (click)=\"saveTest()\" >\r\n                        <clr-icon shape=\"view-list\"></clr-icon> Si, Guardar\r\n                </button>\r\n        </div>\r\n</clr-modal>\r\n            ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/completeregister/completeregister.component.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/completeregister/completeregister.component.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"login-wrapper\" [style.background]=\"'url(' + urlImg + ')'\"> \r\n    <small class=\"infoImgDay\">{{ infoImg }}</small>\r\n    <div class=\"login\" *ngIf=\"stepRegister === 1\">  \r\n        <section class=\"title\">\r\n            <h3 class=\"welcome\">Registro de usuario</h3> \r\n            <h5 class=\"hint\">Por favor introduce el numero de boleta con que realizaste el pago</h5>\r\n        </section>\r\n        <div class=\"login-group\"> \r\n            <clr-input-container>\r\n                <label class=\"clr-sr-only\">Nro. de boleta</label>\r\n                <input type=\"text\" name=\"nroboleta\" clrInput placeholder=\"Nro. de boleta\" [(ngModel)]=\"dataRegister.nroBoleta\" />\r\n            </clr-input-container> \r\n\r\n            <div class=\"alert alert-danger\" *ngIf=\"showError.nroBoleta\" role=\"alert\">\r\n                <div class=\"alert-items\">\r\n                    <div class=\"alert-item static\">\r\n                        <div class=\"alert-icon-wrapper\">\r\n                            <clr-icon class=\"alert-icon\" shape=\"exclamation-circle\"></clr-icon>\r\n                        </div>\r\n                        <span class=\"alert-text\">\r\n                            Este numero de boleta no se encuentra registrado.\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <button class=\"btn btn-primary\" [disabled]=\"dataRegister.nroBoleta.length < 2\" (click)=\"verificarBoleta()\">Verificar</button> \r\n\r\n        </div>\r\n    </div>\r\n    <div class=\"login\" *ngIf=\"stepRegister === 2\"> \r\n        <section class=\"title\">\r\n            <h3 class=\"welcome\">Hola {{ dataRegister.firstName }}</h3> \r\n            <h5 class=\"hint\">Tu correo es <b>{{ dataRegister.email }}</b>, ahora define tu contraseña</h5>\r\n        </section>\r\n        <div class=\"login-group\"> \r\n            <clr-input-container>\r\n                <label class=\"clr-sr-only\">Contraseña</label>\r\n                <input type=\"password\" name=\"password\" clrInput placeholder=\"Contraseña\" [(ngModel)]=\"dataRegister.password\" />\r\n            </clr-input-container>\r\n            <clr-input-container>\r\n                <label class=\"clr-sr-only\">Repite tu contraseña</label>\r\n                <input type=\"password\" name=\"passwordVerify\" clrInput placeholder=\"Repite tu contraseña\" [(ngModel)]=\"dataRegister.passwordVerify\" />\r\n            </clr-input-container> \r\n            <div class=\"alert alert-danger\" *ngIf=\"showError.password\" role=\"alert\">\r\n                <div class=\"alert-items\">\r\n                    <div class=\"alert-item static\">\r\n                        <div class=\"alert-icon-wrapper\">\r\n                            <clr-icon class=\"alert-icon\" shape=\"exclamation-circle\"></clr-icon>\r\n                        </div>\r\n                        <span class=\"alert-text\">\r\n                                Error, las contraseñas deben coincidir, no se permiten contraseñas en blanco.\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </div> \r\n            <button class=\"btn btn-primary\" (click)=\"createPassword()\">Guardar</button> \r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"login\" *ngIf=\"stepRegister === 3\"> \r\n        <section class=\"title\">\r\n            <h3 class=\"welcome\">Bienvenido</h3> \r\n            <h5 class=\"hint\">Estimado(a) <b> {{ dataRegister.firstName }} {{ dataRegister.lastName }} </b>, ya puedes acceder con tu correo: <b> {{ dataRegister.email }} </b> y la contraseña que definiste.</h5>\r\n        </section>\r\n        <div class=\"login-group\">  \r\n            <button class=\"btn btn-primary\" routerLink=\"/login\" routerLinkActive=\"active\"><clr-icon shape=\"login\"></clr-icon> Iniciar sesión</button> \r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/list-user/list-user.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/list-user/list-user.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1>\r\n  <clr-icon shape=\"administrator\" size=\"20\"></clr-icon> Administracion <clr-icon shape=\"play\" size=\"20\"></clr-icon>\r\n  Usuarios\r\n</h1>\r\n\r\n<div class=\"content-area bg-ca mx-ca br-ca\">\r\n\r\n  <button class=\"btn btn-success\" routerLink=\"/admin/usuarios/nuevo\" routerLinkActive=\"active\">\r\n    <clr-icon shape=\"plus\"></clr-icon>Nuevo\r\n  </button>\r\n\r\n  <clr-datagrid>\r\n    <clr-dg-column>Tipo de Identificación</clr-dg-column>\r\n    <clr-dg-column>Documento de Identificación</clr-dg-column>\r\n    <clr-dg-column>Nombres</clr-dg-column>\r\n    <clr-dg-column>Apelidos</clr-dg-column>\r\n    <clr-dg-column>Teléfono</clr-dg-column>\r\n    <clr-dg-column>Correo Electrónico</clr-dg-column>\r\n    <clr-dg-column>Tipo</clr-dg-column>\r\n    <clr-dg-column>Estado</clr-dg-column>\r\n    <clr-dg-column>Editar</clr-dg-column>\r\n    <clr-dg-column>Inhabilitar</clr-dg-column>\r\n\r\n    <clr-dg-row *ngFor=\"let user of listData\">\r\n      <clr-dg-cell>{{user.identityType | typeIdentity}}</clr-dg-cell>\r\n      <clr-dg-cell>{{user.identity}}</clr-dg-cell>\r\n      <clr-dg-cell>{{user.firstName}}</clr-dg-cell>\r\n      <clr-dg-cell>{{user.lastName}}</clr-dg-cell>\r\n      <clr-dg-cell>{{user.phone}}</clr-dg-cell>\r\n      <clr-dg-cell>{{user.email}}</clr-dg-cell>\r\n      <clr-dg-cell>{{user.type | typeUser }}</clr-dg-cell>\r\n      <clr-dg-cell>\r\n        <clr-icon [style.color]=\"(user.active) ? 'green' : 'red'\" shape=\"user\"></clr-icon>\r\n      </clr-dg-cell>\r\n      <clr-dg-cell>\r\n        <clr-icon shape=\"pencil\" [routerLink]=\"'/admin/usuarios/editar/' + user.id\" routerLinkActive=\"active\">\r\n        </clr-icon>\r\n      </clr-dg-cell>\r\n      <clr-dg-cell>\r\n        <clr-icon *ngIf=\"user.active\" (click)=\"enableuser(user)\" shape=\"ban\"></clr-icon>\r\n      </clr-dg-cell>\r\n    </clr-dg-row>\r\n\r\n    <!-- Inside the full datagrid declaration -->\r\n    <clr-dg-footer>\r\n      <clr-dg-pagination #pagination [clrDgPageSize]=\"10\">\r\n        <clr-dg-page-size [clrPageSizeOptions]=\"[10,20,50,100]\">Usuarios por página</clr-dg-page-size>\r\n        {{pagination.firstItem + 1}} - {{pagination.lastItem + 1}}\r\n        de {{pagination.totalItems}} usuarios\r\n      </clr-dg-pagination>\r\n    </clr-dg-footer>\r\n  </clr-datagrid>\r\n\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/register/register.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/register/register.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1>\r\n  <clr-icon shape=\"administrator\" size=\"20\"></clr-icon> Administracion <clr-icon shape=\"play\" size=\"20\"></clr-icon>\r\n  Registro de Usuario\r\n</h1>\r\n\r\n<div class=\"content-area bg-ca mx-ca br-ca\">\r\n  <button class=\"btn btn-primary\" routerLink=\"/admin/usuarios\" routerLinkActive=\"active\">\r\n    <clr-icon shape=\"list\"></clr-icon>Ir a Listado\r\n  </button>\r\n\r\n  <div class=\"clr-row\">\r\n    <div class=\"clr-col-md-6 crl-row-12\">\r\n      <form clrForm>\r\n        <clr-select-container>\r\n          <label>Tipo Doc.</label>\r\n          <select clrSelect [(ngModel)]=\"registerData.identityType\" name=\"identityType\">\r\n            <option value=\"dni\">DNI</option>\r\n            <option value=\"passport\">Pasaporte</option>\r\n            <option value=\"ce\">Carnet de Extranjeria</option>\r\n          </select>\r\n        </clr-select-container>\r\n        <clr-input-container>\r\n          <label>Número</label>\r\n          <input clrInput type=\"text\" [(ngModel)]=\"registerData.identity\" name=\"identity\" required />\r\n          <clr-control-error>Este campo es necesario</clr-control-error>\r\n        </clr-input-container>\r\n        <clr-input-container>\r\n          <label>Nombres</label>\r\n          <input clrInput type=\"text\" [(ngModel)]=\"registerData.firstName\" name=\"firstName\" required />\r\n          <clr-control-error>Este campo es necesario</clr-control-error>\r\n        </clr-input-container>\r\n        <clr-input-container>\r\n          <label>Apellidos</label>\r\n          <input clrInput type=\"text\" [(ngModel)]=\"registerData.lastName\" name=\"lastName\" />\r\n          <clr-control-error>Este campo es necesario</clr-control-error>\r\n        </clr-input-container>\r\n        <clr-input-container>\r\n          <label>Teléfono</label>\r\n          <input clrInput type=\"text\" [(ngModel)]=\"registerData.phone\" name=\"phone\" />\r\n          <clr-control-error>Este campo es necesario</clr-control-error>\r\n        </clr-input-container>\r\n        <clr-input-container>\r\n          <label>Correo Electrónico</label>\r\n          <input clrInput type=\"text\" [(ngModel)]=\"registerData.email\" name=\"email\" />\r\n          <clr-control-error>Este campo es necesario</clr-control-error>\r\n        </clr-input-container>\r\n\r\n        <clr-textarea-container>\r\n          <label>Comentario</label>\r\n          <textarea clrTextarea [(ngModel)]=\"registerData.comment\" name=\"comment\"></textarea>\r\n        </clr-textarea-container>\r\n\r\n        <div class=\"clr-form-control clr-row\">\r\n          <label class=\"clr-control-label clr-col-12 clr-col-md-2\">Vincular Cursos</label>\r\n          <clr-dropdown class=\"clr-control-container clr-col-md-10 clr-col-12\">\r\n            <button class=\"btn btn-outline-primary\" clrDropdownTrigger>\r\n              Seleccionar Curso\r\n              <clr-icon shape=\"caret down\"></clr-icon>\r\n            </button>\r\n            <clr-dropdown-menu clrPosition=\"top-left\" *clrIfOpen>\r\n              <div clrDropdownItem *ngFor=\"let area of areasData\" (click)=\"storeArea(area)\">{{ area.name }}</div>\r\n            </clr-dropdown-menu>\r\n\r\n            <ul class=\"list\">\r\n              <li *ngFor=\"let area of registerData.areas\"> {{ area.name_area }} <clr-icon (click)=\"deleteArea(area)\"\r\n                  shape=\"trash\"></clr-icon>\r\n              </li>\r\n            </ul>\r\n\r\n          </clr-dropdown>\r\n        </div>\r\n\r\n        <clr-select-container *ngIf=\"registerData.id\">\r\n          <label>Estatus</label>\r\n          <select clrSelect [(ngModel)]=\"registerData.active\" name=\"indetityType\">\r\n            <option [ngValue]=\"1\">Activo</option>\r\n            <option [ngValue]=\"0\">Inactivo</option>\r\n          </select>\r\n        </clr-select-container>\r\n\r\n        <clr-select-container>\r\n          <label>Tipo Usuario</label>\r\n          <select clrSelect [(ngModel)]=\"registerData.type\" name=\"indetityType\">\r\n            <option [ngValue]=\"1\">Administrador / Profesor</option>\r\n            <option [ngValue]=\"2\">Estandar</option>\r\n            <option [ngValue]=\"3\">Profesor</option>\r\n          </select>\r\n        </clr-select-container>\r\n        <clr-input-container *ngIf=\"(registerData.type === 1 || registerData.type === 3) && registerData.id === ''\">\r\n          <label>Contraseña de usuario</label>\r\n          <input clrInput type=\"password\" [(ngModel)]=\"thePassword\" name=\"thePassword\" />\r\n          <clr-control-error>Este campo es necesario</clr-control-error>\r\n        </clr-input-container>\r\n\r\n        <div class=\"pt-1\">\r\n          <button class=\"btn btn-success\" (click)=\"store()\">\r\n            <clr-icon shape=\"floppy\"></clr-icon> Guardar Usuario\r\n          </button>\r\n        </div>\r\n\r\n\r\n      </form>\r\n    </div>\r\n    <div class=\"clr-col-md-6 crl-row-12\" *ngIf=\"this.registerData.id && registerData.type === 2\">\r\n\r\n      <form clrForm>\r\n        <h3>Registro de pagos</h3>\r\n        <clr-input-container>\r\n          <label>Nro de Boleta</label>\r\n          <input clrInput type=\"text\" [(ngModel)]=\"registerPay.ticket\" name=\"ticket\" required />\r\n          <clr-control-error>Este campo es necesario</clr-control-error>\r\n        </clr-input-container>\r\n\r\n        <clr-textarea-container>\r\n          <label>Nota</label>\r\n          <textarea clrTextarea [(ngModel)]=\"registerPay.comment\" name=\"comment\"></textarea>\r\n        </clr-textarea-container>\r\n\r\n        <button class=\"btn btn-success\" (click)=\"addPay()\">\r\n          <clr-icon shape=\"plus\"></clr-icon> Agregar Pago\r\n        </button>\r\n        <div>\r\n          <ul class=\"list\">\r\n            <li *ngFor=\"let pago of listPays\"> {{ pago.ticket }} - {{ pago.comment }}<clr-icon (click)=\"deletePay(pago)\"\r\n                shape=\"trash\"></clr-icon>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </form>\r\n    </div>\r\n  </div>\r\n\r\n\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var _components_shared_login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/shared/login/login.component */ "./src/app/components/shared/login/login.component.ts");
/* harmony import */ var _components_admin_dashboard_admin_dashboard_admin_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/admin/dashboard-admin/dashboard-admin.component */ "./src/app/components/admin/dashboard-admin/dashboard-admin.component.ts");
/* harmony import */ var _shared_guard_secure_inner_pages_guard_ts_guard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shared/guard/secure-inner-pages.guard.ts.guard */ "./src/app/shared/guard/secure-inner-pages.guard.ts.guard.ts");
/* harmony import */ var _shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared/guard/auth.guard */ "./src/app/shared/guard/auth.guard.ts");
/* harmony import */ var _components_user_register_register_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/user/register/register.component */ "./src/app/components/user/register/register.component.ts");
/* harmony import */ var _components_user_completeregister_completeregister_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/user/completeregister/completeregister.component */ "./src/app/components/user/completeregister/completeregister.component.ts");
/* harmony import */ var _components_areas_list_areas_list_areas_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/areas/list-areas/list-areas.component */ "./src/app/components/areas/list-areas/list-areas.component.ts");
/* harmony import */ var _components_areas_form_areas_form_areas_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/areas/form-areas/form-areas.component */ "./src/app/components/areas/form-areas/form-areas.component.ts");
/* harmony import */ var _components_user_list_user_list_user_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/user/list-user/list-user.component */ "./src/app/components/user/list-user/list-user.component.ts");
/* harmony import */ var _components_test_user_test_user_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/test-user/test-user.component */ "./src/app/components/test-user/test-user.component.ts");
/* harmony import */ var _components_test_list_test_list_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/test-list/test-list.component */ "./src/app/components/test-list/test-list.component.ts");
/* harmony import */ var _components_test_list_teacher_test_list_teacher_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/test-list-teacher/test-list-teacher.component */ "./src/app/components/test-list-teacher/test-list-teacher.component.ts");
/* harmony import */ var _components_test_evaluate_teacher_test_evaluate_teacher_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/test-evaluate-teacher/test-evaluate-teacher.component */ "./src/app/components/test-evaluate-teacher/test-evaluate-teacher.component.ts");

















const routes = [
    {
        path: '',
        component: _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"],
        canActivate: [_shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'login',
        component: _components_shared_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"],
        canActivate: [_shared_guard_secure_inner_pages_guard_ts_guard__WEBPACK_IMPORTED_MODULE_6__["SecureInnerPagesGuard"]]
    },
    {
        path: 'registro',
        component: _components_user_completeregister_completeregister_component__WEBPACK_IMPORTED_MODULE_9__["CompleteregisterComponent"],
        canActivate: [_shared_guard_secure_inner_pages_guard_ts_guard__WEBPACK_IMPORTED_MODULE_6__["SecureInnerPagesGuard"]]
    },
    {
        path: 'test',
        children: [
            { path: '', component: _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"] },
            { path: 'realizar/:id', component: _components_test_user_test_user_component__WEBPACK_IMPORTED_MODULE_13__["TestUserComponent"] },
            { path: 'detalle/:id', component: _components_test_user_test_user_component__WEBPACK_IMPORTED_MODULE_13__["TestUserComponent"] },
            { path: 'usuario', component: _components_test_list_test_list_component__WEBPACK_IMPORTED_MODULE_14__["TestListComponent"] },
            { path: 'usuario/:tab', component: _components_test_list_test_list_component__WEBPACK_IMPORTED_MODULE_14__["TestListComponent"] },
        ],
        canActivate: [_shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'profesor',
        children: [
            { path: '', component: _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"] },
            { path: 'test/lista', component: _components_test_list_teacher_test_list_teacher_component__WEBPACK_IMPORTED_MODULE_15__["TestListTeacherComponent"] },
            { path: 'test/lista/:tab', component: _components_test_list_teacher_test_list_teacher_component__WEBPACK_IMPORTED_MODULE_15__["TestListTeacherComponent"] },
            { path: 'test/detalle/:id', component: _components_test_evaluate_teacher_test_evaluate_teacher_component__WEBPACK_IMPORTED_MODULE_16__["TestEvaluateTeacherComponent"] },
            { path: 'test/evaluar/:id', component: _components_test_evaluate_teacher_test_evaluate_teacher_component__WEBPACK_IMPORTED_MODULE_16__["TestEvaluateTeacherComponent"] },
        ],
        canActivate: [_shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'admin',
        children: [
            { path: '', component: _components_admin_dashboard_admin_dashboard_admin_component__WEBPACK_IMPORTED_MODULE_5__["DashboardAdminComponent"] },
            { path: 'usuarios', component: _components_user_list_user_list_user_component__WEBPACK_IMPORTED_MODULE_12__["ListUserComponent"] },
            { path: 'usuarios/nuevo', component: _components_user_register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"] },
            { path: 'usuarios/editar/:id', component: _components_user_register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"] },
            { path: 'areas', component: _components_areas_list_areas_list_areas_component__WEBPACK_IMPORTED_MODULE_10__["ListAreasComponent"] },
            { path: 'areas/nuevo', component: _components_areas_form_areas_form_areas_component__WEBPACK_IMPORTED_MODULE_11__["FormAreasComponent"] },
            { path: 'areas/editar/:id', component: _components_areas_form_areas_form_areas_component__WEBPACK_IMPORTED_MODULE_11__["FormAreasComponent"] },
        ],
        canActivate: [_shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: '**',
        redirectTo: ''
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);

/*{
  path: 'prueba/:idprueba',
  component: PruebaComponent,
  canActivate: [SecureInnerPagesGuard]
}*/ 


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_authservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/authservice.service */ "./src/app/services/authservice.service.ts");
/* harmony import */ var _services_generalservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/generalservice.service */ "./src/app/services/generalservice.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");





let AppComponent = class AppComponent {
    constructor(authservice, generalservice, titleService) {
        this.authservice = authservice;
        this.generalservice = generalservice;
        this.titleService = titleService;
        this.title = 'plataforma-evaluacion';
        this.alert = { show: false, text: '', type: '', icon: '' };
    }
    ngOnInit() {
        this.titleService.setTitle('ESCONFA - Plataforma de Evaluación');
        //console.log(this.authservice.isLoggedIn);
        this.generalservice.eventShowAlert.subscribe((options) => {
            console.log(options);
            this.showAlerts(options);
        });
    }
    showAlerts(options) {
        this.alert = Object.assign({ show: true }, options);
        setTimeout(() => {
            this.alert.show = false;
        }, 3000);
    }
};
AppComponent.ctorParameters = () => [
    { type: _services_authservice_service__WEBPACK_IMPORTED_MODULE_2__["AuthserviceService"] },
    { type: _services_generalservice_service__WEBPACK_IMPORTED_MODULE_3__["GeneralserviceService"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _clr_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @clr/angular */ "./node_modules/@clr/angular/fesm2015/clr-angular.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/es2015/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/es2015/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _components_shared_header_header_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/shared/header/header.component */ "./src/app/components/shared/header/header.component.ts");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var _components_shared_login_login_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/shared/login/login.component */ "./src/app/components/shared/login/login.component.ts");
/* harmony import */ var _components_admin_dashboard_admin_dashboard_admin_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/admin/dashboard-admin/dashboard-admin.component */ "./src/app/components/admin/dashboard-admin/dashboard-admin.component.ts");
/* harmony import */ var _components_user_register_register_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/user/register/register.component */ "./src/app/components/user/register/register.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _components_user_completeregister_completeregister_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/user/completeregister/completeregister.component */ "./src/app/components/user/completeregister/completeregister.component.ts");
/* harmony import */ var _components_shared_alert_alert_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/shared/alert/alert.component */ "./src/app/components/shared/alert/alert.component.ts");
/* harmony import */ var _components_areas_list_areas_list_areas_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/areas/list-areas/list-areas.component */ "./src/app/components/areas/list-areas/list-areas.component.ts");
/* harmony import */ var _components_areas_form_areas_form_areas_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/areas/form-areas/form-areas.component */ "./src/app/components/areas/form-areas/form-areas.component.ts");
/* harmony import */ var _components_user_list_user_list_user_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/user/list-user/list-user.component */ "./src/app/components/user/list-user/list-user.component.ts");
/* harmony import */ var _pipes_type_identity_pipe__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./pipes/type-identity.pipe */ "./src/app/pipes/type-identity.pipe.ts");
/* harmony import */ var _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @ckeditor/ckeditor5-angular */ "./node_modules/@ckeditor/ckeditor5-angular/fesm2015/ckeditor-ckeditor5-angular.js");
/* harmony import */ var _components_test_user_test_user_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./components/test-user/test-user.component */ "./src/app/components/test-user/test-user.component.ts");
/* harmony import */ var _components_test_list_test_list_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./components/test-list/test-list.component */ "./src/app/components/test-list/test-list.component.ts");
/* harmony import */ var _components_test_list_teacher_test_list_teacher_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./components/test-list-teacher/test-list-teacher.component */ "./src/app/components/test-list-teacher/test-list-teacher.component.ts");
/* harmony import */ var _components_test_evaluate_teacher_test_evaluate_teacher_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./components/test-evaluate-teacher/test-evaluate-teacher.component */ "./src/app/components/test-evaluate-teacher/test-evaluate-teacher.component.ts");
/* harmony import */ var _pipes_type_user_pipe__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./pipes/type-user.pipe */ "./src/app/pipes/type-user.pipe.ts");
/* harmony import */ var _components_menu_mobile_menu_mobile_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./components/menu-mobile/menu-mobile.component */ "./src/app/components/menu-mobile/menu-mobile.component.ts");
































let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _components_shared_header_header_component__WEBPACK_IMPORTED_MODULE_13__["HeaderComponent"],
            _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_14__["DashboardComponent"],
            _components_shared_login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"],
            _components_admin_dashboard_admin_dashboard_admin_component__WEBPACK_IMPORTED_MODULE_16__["DashboardAdminComponent"],
            _components_user_register_register_component__WEBPACK_IMPORTED_MODULE_17__["RegisterComponent"],
            _components_user_completeregister_completeregister_component__WEBPACK_IMPORTED_MODULE_19__["CompleteregisterComponent"],
            _components_shared_alert_alert_component__WEBPACK_IMPORTED_MODULE_20__["AlertComponent"],
            _components_areas_list_areas_list_areas_component__WEBPACK_IMPORTED_MODULE_21__["ListAreasComponent"],
            _components_areas_form_areas_form_areas_component__WEBPACK_IMPORTED_MODULE_22__["FormAreasComponent"],
            _components_user_list_user_list_user_component__WEBPACK_IMPORTED_MODULE_23__["ListUserComponent"],
            _pipes_type_identity_pipe__WEBPACK_IMPORTED_MODULE_24__["TypeIdentityPipe"],
            _components_test_user_test_user_component__WEBPACK_IMPORTED_MODULE_26__["TestUserComponent"],
            _components_test_list_test_list_component__WEBPACK_IMPORTED_MODULE_27__["TestListComponent"],
            _components_test_list_teacher_test_list_teacher_component__WEBPACK_IMPORTED_MODULE_28__["TestListTeacherComponent"],
            _components_test_evaluate_teacher_test_evaluate_teacher_component__WEBPACK_IMPORTED_MODULE_29__["TestEvaluateTeacherComponent"],
            _pipes_type_user_pipe__WEBPACK_IMPORTED_MODULE_30__["TypeUserPipe"],
            _components_menu_mobile_menu_mobile_component__WEBPACK_IMPORTED_MODULE_31__["MenuMobileComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _clr_angular__WEBPACK_IMPORTED_MODULE_5__["ClarityModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
            _angular_fire__WEBPACK_IMPORTED_MODULE_7__["AngularFireModule"].initializeApp(src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_11__["environment"].firebase),
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__["AngularFirestoreModule"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_10__["AngularFireAuthModule"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorageModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_12__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_18__["FormsModule"],
            _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_25__["CKEditorModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/components/admin/dashboard-admin/dashboard-admin.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/components/admin/dashboard-admin/dashboard-admin.component.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vZGFzaGJvYXJkLWFkbWluL2Rhc2hib2FyZC1hZG1pbi5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/components/admin/dashboard-admin/dashboard-admin.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/admin/dashboard-admin/dashboard-admin.component.ts ***!
  \*******************************************************************************/
/*! exports provided: DashboardAdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardAdminComponent", function() { return DashboardAdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let DashboardAdminComponent = class DashboardAdminComponent {
    constructor() { }
    ngOnInit() {
    }
};
DashboardAdminComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard-admin',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard-admin.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/admin/dashboard-admin/dashboard-admin.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dashboard-admin.component.scss */ "./src/app/components/admin/dashboard-admin/dashboard-admin.component.scss")).default]
    })
], DashboardAdminComponent);



/***/ }),

/***/ "./src/app/components/areas/form-areas/form-areas.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/areas/form-areas/form-areas.component.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".angular-editor-toolbar .angular-editor-toolbar-set .angular-editor-button {\n  padding: 5px !important;\n}\n\n.fa {\n  font-size: 14px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hcmVhcy9mb3JtLWFyZWFzL0M6XFxVc2Vyc1xcZUJpb2xpYnJvc1xcRG9jdW1lbnRzXFxwbGF0YWZvcm1hLWNlc2lhL3NyY1xcYXBwXFxjb21wb25lbnRzXFxhcmVhc1xcZm9ybS1hcmVhc1xcZm9ybS1hcmVhcy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tcG9uZW50cy9hcmVhcy9mb3JtLWFyZWFzL2Zvcm0tYXJlYXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyx1QkFBQTtBQ0NEOztBREVBO0VBQ0ksMEJBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYXJlYXMvZm9ybS1hcmVhcy9mb3JtLWFyZWFzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFuZ3VsYXItZWRpdG9yLXRvb2xiYXIgLmFuZ3VsYXItZWRpdG9yLXRvb2xiYXItc2V0IC5hbmd1bGFyLWVkaXRvci1idXR0b257XHJcbiBwYWRkaW5nOiA1cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmZhIHtcclxuICAgIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xyXG59IiwiLmFuZ3VsYXItZWRpdG9yLXRvb2xiYXIgLmFuZ3VsYXItZWRpdG9yLXRvb2xiYXItc2V0IC5hbmd1bGFyLWVkaXRvci1idXR0b24ge1xuICBwYWRkaW5nOiA1cHggIWltcG9ydGFudDtcbn1cblxuLmZhIHtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/components/areas/form-areas/form-areas.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/areas/form-areas/form-areas.component.ts ***!
  \*********************************************************************/
/*! exports provided: FormAreasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormAreasComponent", function() { return FormAreasComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_areaservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/areaservice.service */ "./src/app/services/areaservice.service.ts");
/* harmony import */ var src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/generalservice.service */ "./src/app/services/generalservice.service.ts");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var src_app_services_banks_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/banks.service */ "./src/app/services/banks.service.ts");







let FormAreasComponent = class FormAreasComponent {
    constructor(route, areasservice, banksservice, generalservice, router) {
        this.route = route;
        this.areasservice = areasservice;
        this.banksservice = banksservice;
        this.generalservice = generalservice;
        this.router = router;
        this.configEditor = { language: 'es' };
        this.areaData = {
            id: "",
            name: "",
            description: "",
            test: []
        };
        this.newTest = {
            name: "",
            description: "",
            maxScore: 20,
            nroQuestions: 10,
            id_bank: ''
        };
        this.listTestDelete = new Array();
        this.bankSelect = {
            questions: []
        };
        this.questionData = {
            type: 'open',
            text: '',
            id: ''
        };
        this.disableForm = false;
        this.disableFormTest = false;
        this.inNewTest = false;
        this.inNewQuestion = false;
        this.modalDeletePrueba = false;
        this.modalConfirmDelete = false;
        this.modalQuestion = false;
        this.modalConfirmDeleteQuestion = false;
        this.inBank = false;
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_5__;
    }
    ngOnInit() {
        if (this.route.snapshot.paramMap.get("id") !== null) {
            this.areaData.id = this.route.snapshot.paramMap.get("id");
            this.loadData();
        }
    }
    loadData() {
        this.areasservice.get(this.areaData.id).subscribe(data => {
            this.areaData = data.data();
        });
    }
    update() {
        this.areasservice.store(this.areaData).then(data => {
            this.generalservice.eventShowAlert.emit({
                text: "Curso guardada exitosamente",
                type: "success"
            });
        }, error => { });
    }
    store() {
        if (this.areaData.id) {
            this.listTestDelete.forEach(test => {
                this.banksservice.delete(test.id);
            });
            this.listTestDelete = [];
            this.areasservice.update(this.areaData).then(() => {
                this.generalservice.eventShowAlert.emit({
                    text: "Curso actualizada correctamente",
                    type: "success"
                });
            }, error => {
                this.generalservice.eventShowAlert.emit({
                    text: "Error en actualización de Curso",
                    type: "danger"
                });
            });
        }
        else {
            this.areasservice.store(this.areaData).then(data => {
                this.generalservice.eventShowAlert.emit({
                    text: "Curso guardada exitosamente",
                    type: "success"
                });
            }, error => {
                this.generalservice.eventShowAlert.emit({
                    text: "Error en actualización de Curso",
                    type: "danger"
                });
            });
        }
    }
    deleteArea() {
        this.modalDeletePrueba = true;
    }
    confirmDeleteArea() {
        this.areasservice.delete(this.areaData.id).then(() => {
            this.generalservice.eventShowAlert.emit({
                text: "Se ha eliminado el curso correctamente",
                type: "success"
            });
            this.router.navigate(['admin', 'areas']);
        }, error => {
            this.generalservice.eventShowAlert.emit({
                text: "Ha ocurrido un error eliminando esta curso",
                type: "error"
            });
        });
    }
    loadDetailTest(test, index) {
        this.newTest = test;
        this.disableForm = true;
        this.inNewTest = false;
        this.indexTestEdit = index;
    }
    formNewTest() {
        this.disableForm = true;
        this.inNewTest = true;
    }
    storeTest(test) {
        if (this.inNewTest) {
            this.areaData.test.push(test);
        }
        else {
            this.areaData.test[this.indexTestEdit] = test;
        }
        this.cancelTest();
    }
    deleteTest() {
        this.modalConfirmDelete = true;
    }
    confirmDeleteTest() {
        let index = this.areaData.test.indexOf(this.newTest);
        this.areaData.test.splice(index, 1);
        this.listTestDelete[this.listTestDelete.length] = this.newTest;
        this.cancelTest();
        this.modalConfirmDelete = false;
    }
    cancelTest() {
        this.disableForm = false;
        this.inNewTest = false;
        this.newTest = {
            name: "",
            description: "",
            maxScore: 20,
            nroQuestions: 10,
            id_bank: ''
        };
    }
    loadBank() {
        this.banksservice.get(this.newTest.id_bank).subscribe(data => {
            this.bankSelect = data.data();
            this.inBank = true;
            this.disableFormTest = true;
            this.scrollBank();
        });
    }
    saveBank() {
        this.banksservice.update(this.bankSelect).then(() => {
            this.generalservice.eventShowAlert.emit({
                text: "Se han guardado correctamente las preguntas",
                type: "success"
            });
        }, error => {
            this.generalservice.eventShowAlert.emit({
                text: "Ha ocurrido un error modificando el banco de preguntas, intente nuevamente.",
                type: "error"
            });
        });
    }
    closeBank() {
        this.inBank = false;
        this.disableFormTest = false;
    }
    scrollBank() {
        let el = document.getElementById('bankSector');
        el.scrollIntoView();
        this.bankSectorHTML.nativeElement.scrollIntoView();
    }
    addQuestion() {
        this.inNewQuestion = true;
        this.questionData = {
            type: 'open',
            text: '',
            id: ''
        };
        this.modalQuestion = true;
    }
    editQuestion(question, index) {
        this.questionData = question;
        this.modalQuestion = true;
        this.inNewQuestion = false;
        this.indexQuestionEdit = index;
    }
    confirmAddQuestion() {
        if (this.inNewQuestion) {
            this.bankSelect.questions.push(this.questionData);
        }
        else {
            this.bankSelect.questions[this.indexQuestionEdit] = this.questionData;
        }
        this.cancelAddQuestion();
    }
    cancelAddQuestion() {
        this.modalQuestion = false;
        this.resetDataQuestion();
    }
    resetDataQuestion() {
        this.questionData = {
            type: 'open',
            text: '',
            id: ''
        };
    }
    deleteQuestion(question) {
        this.modalConfirmDeleteQuestion = true;
        this.questionData = question;
    }
    cancelDeleteQuestion() {
        this.modalConfirmDeleteQuestion = false;
        this.resetDataQuestion();
    }
    confirmDeleteQuestion() {
        let index = this.bankSelect.questions.indexOf(this.questionData);
        this.bankSelect.questions.splice(index, 1);
        this.modalConfirmDeleteQuestion = false;
        this.resetDataQuestion();
    }
};
FormAreasComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_services_areaservice_service__WEBPACK_IMPORTED_MODULE_3__["AreaserviceService"] },
    { type: src_app_services_banks_service__WEBPACK_IMPORTED_MODULE_6__["BanksService"] },
    { type: src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_4__["GeneralserviceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("bankSector", { static: false })
], FormAreasComponent.prototype, "bankSectorHTML", void 0);
FormAreasComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-form-areas",
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./form-areas.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/areas/form-areas/form-areas.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./form-areas.component.scss */ "./src/app/components/areas/form-areas/form-areas.component.scss")).default]
    })
], FormAreasComponent);



/***/ }),

/***/ "./src/app/components/areas/list-areas/list-areas.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/areas/list-areas/list-areas.component.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYXJlYXMvbGlzdC1hcmVhcy9saXN0LWFyZWFzLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/components/areas/list-areas/list-areas.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/areas/list-areas/list-areas.component.ts ***!
  \*********************************************************************/
/*! exports provided: ListAreasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListAreasComponent", function() { return ListAreasComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_areaservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/areaservice.service */ "./src/app/services/areaservice.service.ts");



let ListAreasComponent = class ListAreasComponent {
    constructor(areasservice) {
        this.areasservice = areasservice;
    }
    ngOnInit() {
        this.load();
    }
    load() {
        this.areasservice.list().subscribe(data => {
            this.areasData = data;
        });
    }
};
ListAreasComponent.ctorParameters = () => [
    { type: src_app_services_areaservice_service__WEBPACK_IMPORTED_MODULE_2__["AreaserviceService"] }
];
ListAreasComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-areas',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./list-areas.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/areas/list-areas/list-areas.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./list-areas.component.scss */ "./src/app/components/areas/list-areas/list-areas.component.scss")).default]
    })
], ListAreasComponent);



/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.ts ***!
  \*************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/authservice.service */ "./src/app/services/authservice.service.ts");



let DashboardComponent = class DashboardComponent {
    constructor(authservice) {
        this.authservice = authservice;
    }
    ngOnInit() {
        this.getUserData();
        this.mostrarSaludo();
    }
    getUserData() {
        this.userData = this.authservice.getUserData();
    }
    mostrarSaludo() {
        let fecha = new Date();
        let hora = fecha.getHours();
        if (hora >= 0 && hora < 12) {
            this.textSaludo = "buenos días";
        }
        if (hora >= 12 && hora < 18) {
            this.textSaludo = "buenas tardes";
        }
        if (hora >= 18 && hora < 24) {
            this.textSaludo = "buenas noches";
        }
    }
};
DashboardComponent.ctorParameters = () => [
    { type: src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_2__["AuthserviceService"] }
];
DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/dashboard/dashboard.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/components/dashboard/dashboard.component.scss")).default]
    })
], DashboardComponent);



/***/ }),

/***/ "./src/app/components/menu-mobile/menu-mobile.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/components/menu-mobile/menu-mobile.component.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbWVudS1tb2JpbGUvbWVudS1tb2JpbGUuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/components/menu-mobile/menu-mobile.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/menu-mobile/menu-mobile.component.ts ***!
  \*****************************************************************/
/*! exports provided: MenuMobileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuMobileComponent", function() { return MenuMobileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/authservice.service */ "./src/app/services/authservice.service.ts");



let MenuMobileComponent = class MenuMobileComponent {
    constructor(authservice) {
        this.authservice = authservice;
        this.userLoad = false;
        this.userData = {
            identity: '',
            identityType: '',
            email: '',
            firstName: '',
            lastName: '',
            areas: []
        };
    }
    ngOnInit() {
        this.getUserData();
    }
    getUserData() {
        setTimeout(() => {
            this.userLoad = true;
            this.userData = this.authservice.getUserData();
        }, 1000);
    }
};
MenuMobileComponent.ctorParameters = () => [
    { type: src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_2__["AuthserviceService"] }
];
MenuMobileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-menu-mobile',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./menu-mobile.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/menu-mobile/menu-mobile.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./menu-mobile.component.scss */ "./src/app/components/menu-mobile/menu-mobile.component.scss")).default]
    })
], MenuMobileComponent);



/***/ }),

/***/ "./src/app/components/shared/alert/alert.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/components/shared/alert/alert.component.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2hhcmVkL2FsZXJ0L2FsZXJ0LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/components/shared/alert/alert.component.ts":
/*!************************************************************!*\
  !*** ./src/app/components/shared/alert/alert.component.ts ***!
  \************************************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return AlertComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AlertComponent = class AlertComponent {
    constructor() { }
    ngOnInit() {
        console.log(this.options);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], AlertComponent.prototype, "options", void 0);
AlertComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-alert',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./alert.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/alert/alert.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./alert.component.scss */ "./src/app/components/shared/alert/alert.component.scss")).default]
    })
], AlertComponent);



/***/ }),

/***/ "./src/app/components/shared/header/header.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/components/shared/header/header.component.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2hhcmVkL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/components/shared/header/header.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/shared/header/header.component.ts ***!
  \**************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/authservice.service */ "./src/app/services/authservice.service.ts");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");




let HeaderComponent = class HeaderComponent {
    constructor(authservice) {
        this.authservice = authservice;
        this.userLoad = false;
        this.userData = {
            identity: '',
            identityType: '',
            email: '',
            firstName: '',
            lastName: '',
            areas: []
        };
    }
    ngOnInit() {
        this.getUserData();
        this.name_company = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["environment"].name_company;
    }
    getUserData() {
        setTimeout(() => {
            this.userLoad = true;
            this.userData = this.authservice.getUserData();
        }, 1000);
    }
};
HeaderComponent.ctorParameters = () => [
    { type: src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_2__["AuthserviceService"] }
];
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/header/header.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header.component.scss */ "./src/app/components/shared/header/header.component.scss")).default]
    })
], HeaderComponent);



/***/ }),

/***/ "./src/app/components/shared/login/login.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/components/shared/login/login.component.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".infoImgDay {\n  position: absolute;\n  bottom: 10px;\n  right: 10px;\n  color: #bbbbbb;\n  width: 300px;\n  text-align: right;\n}\n\n.login-wrapper .login .login-group .btn {\n  margin-top: 0.5rem;\n}\n\n.errorLogin,\n.successLogin {\n  color: #d20f0f;\n  background: white;\n  padding: 8px;\n  text-align: center;\n}\n\n.successLogin {\n  color: green;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zaGFyZWQvbG9naW4vQzpcXFVzZXJzXFxlQmlvbGlicm9zXFxEb2N1bWVudHNcXHBsYXRhZm9ybWEtY2VzaWEvc3JjXFxhcHBcXGNvbXBvbmVudHNcXHNoYXJlZFxcbG9naW5cXGxvZ2luLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL3NoYXJlZC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FDQ0Y7O0FERUE7RUFDRSxrQkFBQTtBQ0NGOztBREVBOztFQUVFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ0NGOztBREVBO0VBQ0UsWUFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9zaGFyZWQvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW5mb0ltZ0RheSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvdHRvbTogMTBweDtcclxuICByaWdodDogMTBweDtcclxuICBjb2xvcjogI2JiYmJiYjtcclxuICB3aWR0aDogMzAwcHg7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuXHJcbi5sb2dpbi13cmFwcGVyIC5sb2dpbiAubG9naW4tZ3JvdXAgLmJ0biB7XHJcbiAgbWFyZ2luLXRvcDogLjVyZW07XHJcbn1cclxuXHJcbi5lcnJvckxvZ2luLFxyXG4uc3VjY2Vzc0xvZ2luIHtcclxuICBjb2xvcjogI2QyMGYwZjtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBwYWRkaW5nOiA4cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uc3VjY2Vzc0xvZ2luIHtcclxuICBjb2xvcjogZ3JlZW47XHJcbn1cclxuIiwiLmluZm9JbWdEYXkge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMTBweDtcbiAgcmlnaHQ6IDEwcHg7XG4gIGNvbG9yOiAjYmJiYmJiO1xuICB3aWR0aDogMzAwcHg7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4ubG9naW4td3JhcHBlciAubG9naW4gLmxvZ2luLWdyb3VwIC5idG4ge1xuICBtYXJnaW4tdG9wOiAwLjVyZW07XG59XG5cbi5lcnJvckxvZ2luLFxuLnN1Y2Nlc3NMb2dpbiB7XG4gIGNvbG9yOiAjZDIwZjBmO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogOHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5zdWNjZXNzTG9naW4ge1xuICBjb2xvcjogZ3JlZW47XG59Il19 */");

/***/ }),

/***/ "./src/app/components/shared/login/login.component.ts":
/*!************************************************************!*\
  !*** ./src/app/components/shared/login/login.component.ts ***!
  \************************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/generalservice.service */ "./src/app/services/generalservice.service.ts");
/* harmony import */ var src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/authservice.service */ "./src/app/services/authservice.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");






let LoginComponent = class LoginComponent {
    constructor(generalService, authservice, router, ngZone) {
        this.generalService = generalService;
        this.authservice = authservice;
        this.router = router;
        this.ngZone = ngZone;
        this.showError = false;
        this.showWelcome = false;
    }
    ngOnInit() {
        this.getImgDay();
        this.name_company = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].name_company;
    }
    getImgDay() {
        this.generalService.getPicOfDay().subscribe((data) => {
            this.urlImg = 'https://www.bing.com/' + data.images[0].url;
            this.infoImg = data.images[0].copyright;
        });
    }
    logIn(email, password) {
        this.authservice.LogIn(email, password).then((result) => {
            this.showWelcome = true;
        }).catch((error) => {
            this.showError = true;
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_2__["GeneralserviceService"] },
    { type: src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__["AuthserviceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/login/login.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.scss */ "./src/app/components/shared/login/login.component.scss")).default]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/components/test-evaluate-teacher/test-evaluate-teacher.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/components/test-evaluate-teacher/test-evaluate-teacher.component.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGVzdC1ldmFsdWF0ZS10ZWFjaGVyL3Rlc3QtZXZhbHVhdGUtdGVhY2hlci5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/components/test-evaluate-teacher/test-evaluate-teacher.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/test-evaluate-teacher/test-evaluate-teacher.component.ts ***!
  \*************************************************************************************/
/*! exports provided: TestEvaluateTeacherComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestEvaluateTeacherComponent", function() { return TestEvaluateTeacherComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_pruebas_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/pruebas.service */ "./src/app/services/pruebas.service.ts");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/generalservice.service */ "./src/app/services/generalservice.service.ts");
/* harmony import */ var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/users.service */ "./src/app/services/users.service.ts");







let TestEvaluateTeacherComponent = class TestEvaluateTeacherComponent {
    constructor(route, pruebasservice, generalservice, usersservice, router) {
        this.route = route;
        this.pruebasservice = pruebasservice;
        this.generalservice = generalservice;
        this.usersservice = usersservice;
        this.router = router;
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__;
        this.configEditor = { language: 'es', isReadOnly: false };
        this.inLoad = true;
        this.resultSaveTest = {
            msg: '',
            saved: false,
            inSave: false
        };
        this.modalConfirmEvaluate = false;
        this.userTestData = {
            identity: '',
            identityType: '',
            email: '',
            firstName: '',
            lastName: '',
            areas: []
        };
        this.pruebaData = {
            name: 'Prueba',
            id_test: '',
            id_area: '',
            id_user: '',
            questions: [],
            score: 0,
            comments: '',
            status: 0,
            dateRealize: null,
            dateEvaluate: null
        };
    }
    ngOnInit() {
        if (this.route.snapshot.paramMap.get("id") !== null) {
            this.pruebaData.id = this.route.snapshot.paramMap.get("id");
            this.loadData();
        }
    }
    loadData() {
        this.inLoad = true;
        this.pruebasservice.get(this.pruebaData.id).subscribe(data => {
            this.pruebaData = data.data();
            this.getDataUserTest();
            if (this.pruebaData.status === 1 || this.pruebaData.status === 2)
                this.configEditor.isReadOnly = true;
            //console.log(this.pruebaData)
            this.inLoad = false;
        });
    }
    getDataUserTest() {
        this.usersservice.get(this.pruebaData.id_user).subscribe(data => {
            this.userTestData = data.data();
        });
    }
    modalConfirmEvaluateOpen() {
        this.modalConfirmEvaluate = true;
        this.resultSaveTest.msg = '';
        this.resultSaveTest.inSave = false;
        this.resultSaveTest.saved = false;
    }
    saveTestEvaluation() {
        this.resultSaveTest.inSave = true;
        this.pruebaData.status = 2;
        this.pruebaData.dateEvaluate = new Date();
        this.pruebasservice.store(this.pruebaData).then(() => {
            this.resultSaveTest.inSave = false;
            this.resultSaveTest.saved = true;
            this.generalservice.eventShowAlert.emit({
                text: "Se ha guardado la evaluacion exitosamente",
                type: "success"
            });
            this.router.navigate(['profesor', 'test', 'lista']);
        }, error => {
            this.generalservice.eventShowAlert.emit({
                text: "Ha ocurrido un error guardando la evaluacion, intente nuevamente",
                type: "error"
            });
        });
    }
};
TestEvaluateTeacherComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_services_pruebas_service__WEBPACK_IMPORTED_MODULE_3__["PruebasService"] },
    { type: src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_5__["GeneralserviceService"] },
    { type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UsersService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
TestEvaluateTeacherComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-test-evaluate-teacher',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./test-evaluate-teacher.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-evaluate-teacher/test-evaluate-teacher.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./test-evaluate-teacher.component.scss */ "./src/app/components/test-evaluate-teacher/test-evaluate-teacher.component.scss")).default]
    })
], TestEvaluateTeacherComponent);



/***/ }),

/***/ "./src/app/components/test-list-teacher/test-list-teacher.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/components/test-list-teacher/test-list-teacher.component.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGVzdC1saXN0LXRlYWNoZXIvdGVzdC1saXN0LXRlYWNoZXIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/components/test-list-teacher/test-list-teacher.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/test-list-teacher/test-list-teacher.component.ts ***!
  \*****************************************************************************/
/*! exports provided: TestListTeacherComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestListTeacherComponent", function() { return TestListTeacherComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_pruebas_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/pruebas.service */ "./src/app/services/pruebas.service.ts");
/* harmony import */ var src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/authservice.service */ "./src/app/services/authservice.service.ts");
/* harmony import */ var src_app_services_areaservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/areaservice.service */ "./src/app/services/areaservice.service.ts");
/* harmony import */ var src_app_services_banks_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/banks.service */ "./src/app/services/banks.service.ts");
/* harmony import */ var src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/generalservice.service */ "./src/app/services/generalservice.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");
/* harmony import */ var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/users.service */ "./src/app/services/users.service.ts");










let TestListTeacherComponent = class TestListTeacherComponent {
    constructor(authservice, areasservice, bankservice, pruebasservice, generalservice, router, route, usersservice, afs) {
        this.authservice = authservice;
        this.areasservice = areasservice;
        this.bankservice = bankservice;
        this.pruebasservice = pruebasservice;
        this.generalservice = generalservice;
        this.router = router;
        this.route = route;
        this.usersservice = usersservice;
        this.afs = afs;
        this.tabTestPending = true;
        this.tabTestHistory = false;
        this.inLoad = false;
        this.areasTestUser = [];
        this.testPendingData = [];
        this.testHistoryData = [];
        this.modalConfirmTestStart = false;
        this.areaSelData = {
            name: '',
            description: '',
            test: []
        };
        this.testSelData = {
            name: '',
            description: '',
            maxScore: 0,
            nroQuestions: 0,
            id_bank: ''
        };
        this.inGenerateTest = false;
    }
    ngOnInit() {
        this.getUserData();
        if (this.route.snapshot.paramMap.get("tab") !== null) {
            if (this.route.snapshot.paramMap.get("tab") === 'historial') {
                this.tabTestPending = false;
                this.tabTestHistory = true;
                this.getTestsDataHistory();
            }
            else {
                this.getTestsDataPending();
            }
        }
        else {
            this.getTestsDataPending();
        }
    }
    getUserData() {
        this.inLoad = true;
        this.userData = this.authservice.getUserData();
    }
    getTestsDataPending() {
        let subscripcion;
        subscripcion = this.pruebasservice.getTestsPendingEvaluate().subscribe(data => {
            this.testPendingData = data;
            this.testPendingData.forEach((test, index) => {
                this.getDataUserTestP(test.id_user, index);
            });
            //console.log(this.testPendingData)
            this.unsubscribeMethod(subscripcion);
        }, error => {
            this.generalservice.eventShowAlert.emit({
                text: "Error obteniendo historial de pruebas pendientes",
                type: "error"
            });
        });
    }
    getDataUserTestP(userid, index) {
        this.usersservice.get(userid).subscribe(data => {
            this.testPendingData[index].userTestData = data.data();
        });
    }
    getDataUserTestH(userid, index) {
        this.usersservice.get(userid).subscribe(data => {
            this.testHistoryData[index].userTestData = data.data();
        });
    }
    confirmTest(area, test) {
        this.modalConfirmTestStart = true;
        this.areaSelData = area;
        this.testSelData = test;
    }
    saveTestUser(data) {
        let idTest = this.afs.createId();
        data.id = idTest;
        this.pruebasservice.store(data).then(() => {
            this.generalservice.eventShowAlert.emit({
                text: "Prueba generada exitosamente",
                type: "success"
            });
            this.router.navigate(['test', 'realizar', idTest]);
        }, error => {
            this.generalservice.eventShowAlert.emit({
                text: "Error genrando la prueba",
                type: "error"
            });
        });
    }
    getTestsDataHistory() {
        let subscripcion;
        subscripcion = this.pruebasservice.getTestsHistoryEvaluate().subscribe(data => {
            this.testHistoryData = data;
            this.testHistoryData.forEach((test, index) => {
                this.getDataUserTestH(test.id_user, index);
            });
            //console.log(this.testHistoryData)
            this.unsubscribeMethod(subscripcion);
        }, error => {
            this.generalservice.eventShowAlert.emit({
                text: "Error obteniendo historial de pruebas",
                type: "error"
            });
        });
    }
    unsubscribeMethod(suscripcion) {
        suscripcion.unsubscribe();
    }
};
TestListTeacherComponent.ctorParameters = () => [
    { type: src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__["AuthserviceService"] },
    { type: src_app_services_areaservice_service__WEBPACK_IMPORTED_MODULE_4__["AreaserviceService"] },
    { type: src_app_services_banks_service__WEBPACK_IMPORTED_MODULE_5__["BanksService"] },
    { type: src_app_services_pruebas_service__WEBPACK_IMPORTED_MODULE_2__["PruebasService"] },
    { type: src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_6__["GeneralserviceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
    { type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_9__["UsersService"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__["AngularFirestore"] }
];
TestListTeacherComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-test-list-teacher',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./test-list-teacher.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-list-teacher/test-list-teacher.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./test-list-teacher.component.scss */ "./src/app/components/test-list-teacher/test-list-teacher.component.scss")).default]
    })
], TestListTeacherComponent);



/***/ }),

/***/ "./src/app/components/test-list/test-list.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/test-list/test-list.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGVzdC1saXN0L3Rlc3QtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/components/test-list/test-list.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/test-list/test-list.component.ts ***!
  \*************************************************************/
/*! exports provided: TestListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestListComponent", function() { return TestListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_pruebas_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/pruebas.service */ "./src/app/services/pruebas.service.ts");
/* harmony import */ var src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/authservice.service */ "./src/app/services/authservice.service.ts");
/* harmony import */ var src_app_services_areaservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/areaservice.service */ "./src/app/services/areaservice.service.ts");
/* harmony import */ var src_app_services_banks_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/banks.service */ "./src/app/services/banks.service.ts");
/* harmony import */ var src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/generalservice.service */ "./src/app/services/generalservice.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");









let TestListComponent = class TestListComponent {
    constructor(authservice, areasservice, bankservice, pruebasservice, generalservice, router, route, afs) {
        this.authservice = authservice;
        this.areasservice = areasservice;
        this.bankservice = bankservice;
        this.pruebasservice = pruebasservice;
        this.generalservice = generalservice;
        this.router = router;
        this.route = route;
        this.afs = afs;
        this.tabTestPending = true;
        this.tabTestHistory = false;
        this.inLoad = false;
        this.areasTestUser = [];
        this.testHistoryData = [];
        this.modalConfirmTestStart = false;
        this.areaSelData = {
            name: '',
            description: '',
            test: []
        };
        this.testSelData = {
            name: '',
            description: '',
            maxScore: 0,
            nroQuestions: 0,
            id_bank: ''
        };
        this.inGenerateTest = false;
    }
    ngOnInit() {
        this.getUserData();
        if (this.route.snapshot.paramMap.get("tab") !== null) {
            if (this.route.snapshot.paramMap.get("tab") === 'historial') {
                this.tabTestPending = false;
                this.tabTestHistory = true;
                this.loadHistory();
            }
            else {
                this.getTestsData();
            }
        }
        else {
            this.getTestsData();
        }
    }
    getUserData() {
        this.inLoad = true;
        this.userData = this.authservice.getUserData();
    }
    getTestsData() {
        if (this.userData.areas.length > 0) {
            this.userData.areas.forEach((element) => {
                //console.log(element.id_area);
                this.areasservice.get(element.id_area).subscribe(data => {
                    let dataadd = data.data();
                    this.areasTestUser.push(dataadd);
                    this.inLoad = false;
                });
            });
        }
    }
    confirmTest(area, test) {
        this.modalConfirmTestStart = true;
        this.areaSelData = area;
        this.testSelData = test;
    }
    generateTest() {
        this.inGenerateTest = true;
        this.bankservice.get(this.testSelData.id_bank).subscribe(data => {
            let questionData = data.data().questions;
            questionData.forEach((question) => {
                question.score = 0;
                question.answer = '';
            });
            if (questionData.length < this.testSelData.nroQuestions) {
                this.generalservice.eventShowAlert.emit({
                    text: "Esta prueba no puede ser realizada en estos momentos.",
                    type: "info"
                });
                this.modalConfirmTestStart = false;
                this.inGenerateTest = false;
            }
            else {
                let arrayQuestionsRamdon = this.getRandom(questionData, this.testSelData.nroQuestions);
                let testusuarios = {
                    name: this.testSelData.name,
                    id_test: this.testSelData.id,
                    id_area: this.areaSelData.id,
                    id_user: this.userData.id,
                    score: 0,
                    comments: '',
                    questions: arrayQuestionsRamdon,
                    status: 0,
                    dateEvaluate: null,
                    dateRealize: null
                };
                this.saveTestUser(testusuarios);
            }
        }, error => {
            this.generalservice.eventShowAlert.emit({
                text: "Ha ocurrido un error, intente nuevamente",
                type: "error"
            });
        });
    }
    saveTestUser(data) {
        let idTest = this.afs.createId();
        data.id = idTest;
        this.pruebasservice.store(data).then(() => {
            this.generalservice.eventShowAlert.emit({
                text: "Prueba generada exitosamente",
                type: "success"
            });
            this.router.navigate(['test', 'realizar', idTest]);
        }, error => {
            this.generalservice.eventShowAlert.emit({
                text: "Error genrando la prueba",
                type: "error"
            });
        });
    }
    getRandom(arr, n) {
        var result = new Array(n), len = arr.length, taken = new Array(len);
        if (n > len)
            throw new RangeError("getRandom: more elements taken than available");
        while (n--) {
            var x = Math.floor(Math.random() * len);
            result[n] = arr[x in taken ? taken[x] : x];
            taken[x] = --len in taken ? taken[len] : len;
        }
        return result;
    }
    loadHistory() {
        let subscripcion;
        subscripcion = this.pruebasservice.getHistory(this.userData.id).subscribe(data => {
            this.testHistoryData = data;
            console.log(this.testHistoryData);
            this.unsubscribeMethod(subscripcion);
        }, error => {
            this.generalservice.eventShowAlert.emit({
                text: "Error obteniendo historial de pruebas",
                type: "error"
            });
        });
    }
    unsubscribeMethod(suscripcion) {
        suscripcion.unsubscribe();
    }
};
TestListComponent.ctorParameters = () => [
    { type: src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__["AuthserviceService"] },
    { type: src_app_services_areaservice_service__WEBPACK_IMPORTED_MODULE_4__["AreaserviceService"] },
    { type: src_app_services_banks_service__WEBPACK_IMPORTED_MODULE_5__["BanksService"] },
    { type: src_app_services_pruebas_service__WEBPACK_IMPORTED_MODULE_2__["PruebasService"] },
    { type: src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_6__["GeneralserviceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__["AngularFirestore"] }
];
TestListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-test-list',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./test-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-list/test-list.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./test-list.component.scss */ "./src/app/components/test-list/test-list.component.scss")).default]
    })
], TestListComponent);



/***/ }),

/***/ "./src/app/components/test-user/test-user.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/test-user/test-user.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGVzdC11c2VyL3Rlc3QtdXNlci5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/components/test-user/test-user.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/test-user/test-user.component.ts ***!
  \*************************************************************/
/*! exports provided: TestUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestUserComponent", function() { return TestUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_pruebas_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/pruebas.service */ "./src/app/services/pruebas.service.ts");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/generalservice.service */ "./src/app/services/generalservice.service.ts");






let TestUserComponent = class TestUserComponent {
    constructor(route, pruebasservice, generalservice, router) {
        this.route = route;
        this.pruebasservice = pruebasservice;
        this.generalservice = generalservice;
        this.router = router;
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__;
        this.configEditor = { language: 'es', isReadOnly: false };
        this.inLoad = true;
        this.resultSaveTest = {
            msg: '',
            saved: false,
            inSave: false
        };
        this.modalConfirmSaveTest = false;
        this.pruebaData = {
            name: 'Prueba',
            id_test: '',
            id_area: '',
            id_user: '',
            questions: [],
            score: 0,
            comments: '',
            status: 0,
            dateRealize: null,
            dateEvaluate: null
        };
    }
    ngOnInit() {
        if (this.route.snapshot.paramMap.get("id") !== null) {
            this.pruebaData.id = this.route.snapshot.paramMap.get("id");
            this.loadData();
        }
    }
    loadData() {
        this.inLoad = true;
        this.pruebasservice.get(this.pruebaData.id).subscribe(data => {
            this.pruebaData = data.data();
            if (this.pruebaData.status === 1 || this.pruebaData.status === 2)
                this.configEditor.isReadOnly = true;
            //console.log(this.pruebaData)
            this.inLoad = false;
        });
    }
    modalConfirmTest() {
        this.modalConfirmSaveTest = true;
        this.resultSaveTest.msg = '';
        this.resultSaveTest.inSave = false;
        this.resultSaveTest.saved = false;
    }
    saveTest() {
        this.resultSaveTest.inSave = true;
        this.pruebaData.status = 1;
        this.pruebaData.dateRealize = new Date();
        this.pruebasservice.store(this.pruebaData).then(() => {
            this.resultSaveTest.inSave = false;
            this.resultSaveTest.saved = true;
            this.generalservice.eventShowAlert.emit({
                text: "Prueba guardada exitosamente",
                type: "success"
            });
            this.router.navigate(['test', 'usuario', 'historial']);
        }, error => {
            this.generalservice.eventShowAlert.emit({
                text: "Ha ocurrido un error guardando la prueba, intente nuevamente",
                type: "error"
            });
        });
    }
};
TestUserComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_services_pruebas_service__WEBPACK_IMPORTED_MODULE_3__["PruebasService"] },
    { type: src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_5__["GeneralserviceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
TestUserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-test-user',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./test-user.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/test-user/test-user.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./test-user.component.scss */ "./src/app/components/test-user/test-user.component.scss")).default]
    })
], TestUserComponent);



/***/ }),

/***/ "./src/app/components/user/completeregister/completeregister.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/components/user/completeregister/completeregister.component.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".infoImgDay {\n  position: absolute;\n  bottom: 10px;\n  right: 10px;\n  color: #bbbbbb;\n  width: 300px;\n  text-align: right;\n}\n\n.login-wrapper .login .login-group .btn {\n  margin-top: 0.5rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy91c2VyL2NvbXBsZXRlcmVnaXN0ZXIvQzpcXFVzZXJzXFxlQmlvbGlicm9zXFxEb2N1bWVudHNcXHBsYXRhZm9ybWEtY2VzaWEvc3JjXFxhcHBcXGNvbXBvbmVudHNcXHVzZXJcXGNvbXBsZXRlcmVnaXN0ZXJcXGNvbXBsZXRlcmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvdXNlci9jb21wbGV0ZXJlZ2lzdGVyL2NvbXBsZXRlcmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREdBO0VBQ0ksa0JBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlci9jb21wbGV0ZXJlZ2lzdGVyL2NvbXBsZXRlcmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW5mb0ltZ0RheSB7IFxyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAxMHB4O1xyXG4gICAgcmlnaHQ6IDEwcHg7XHJcbiAgICBjb2xvcjogI2JiYmJiYjtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcblxyXG4gICAgXHJcbi5sb2dpbi13cmFwcGVyIC5sb2dpbiAubG9naW4tZ3JvdXAgLmJ0biB7XHJcbiAgICBtYXJnaW4tdG9wOiAuNXJlbTtcclxufSIsIi5pbmZvSW1nRGF5IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDEwcHg7XG4gIHJpZ2h0OiAxMHB4O1xuICBjb2xvcjogI2JiYmJiYjtcbiAgd2lkdGg6IDMwMHB4O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbn1cblxuLmxvZ2luLXdyYXBwZXIgLmxvZ2luIC5sb2dpbi1ncm91cCAuYnRuIHtcbiAgbWFyZ2luLXRvcDogMC41cmVtO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/components/user/completeregister/completeregister.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/components/user/completeregister/completeregister.component.ts ***!
  \********************************************************************************/
/*! exports provided: CompleteregisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompleteregisterComponent", function() { return CompleteregisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/generalservice.service */ "./src/app/services/generalservice.service.ts");
/* harmony import */ var src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/authservice.service */ "./src/app/services/authservice.service.ts");




let CompleteregisterComponent = class CompleteregisterComponent {
    constructor(generalService, authservice) {
        this.generalService = generalService;
        this.authservice = authservice;
        this.dataRegister = {
            nroBoleta: '',
            password: '',
            email: '',
            passwordVerify: '',
            firstName: '',
            lastName: ''
        };
        this.showError = {
            nroBoleta: false,
            password: false
        };
        this.stepRegister = 1;
    }
    ngOnInit() {
        this.getImgDay();
    }
    verificarBoleta() {
        this.authservice.checkNroBoleta(this.dataRegister.nroBoleta).subscribe(result => {
            if (result.docs.length > 0) {
                this.authservice.getUser(result.docs[0].data().id_user).subscribe(result => {
                    if (result.data() !== undefined) {
                        this.dataRegister.email = result.data().email;
                        this.dataRegister.firstName = result.data().firstName;
                        this.dataRegister.lastName = result.data().lastName;
                        this.stepRegister = 2;
                    }
                    else {
                        this.generalService.eventShowAlert.emit({
                            text: "Error procesando el numero de boleta",
                            type: "success"
                        });
                    }
                });
            }
            else {
                this.generalService.eventShowAlert.emit({
                    text: "El numero de boleta no se encuentra registrado",
                    type: "error"
                });
            }
        }, error => {
            console.log(error);
        });
    }
    createPassword() {
        if (this.dataRegister.password !== '' && this.dataRegister.passwordVerify !== '') {
            if (this.dataRegister.password == this.dataRegister.passwordVerify) {
                //console.log(this.dataRegister)
                this.authservice.Register(this.dataRegister.email, this.dataRegister.password);
                this.stepRegister = 3;
            }
            else {
                this.showError.password = true;
            }
        }
        else {
            this.showError.password = true;
        }
    }
    getImgDay() {
        this.generalService.getPicOfDay().subscribe((data) => {
            this.urlImg = 'https://www.bing.com/' + data.images[0].url;
            this.infoImg = data.images[0].copyright;
        });
    }
};
CompleteregisterComponent.ctorParameters = () => [
    { type: src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_2__["GeneralserviceService"] },
    { type: src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__["AuthserviceService"] }
];
CompleteregisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-completeregister',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./completeregister.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/completeregister/completeregister.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./completeregister.component.scss */ "./src/app/components/user/completeregister/completeregister.component.scss")).default]
    })
], CompleteregisterComponent);



/***/ }),

/***/ "./src/app/components/user/list-user/list-user.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/components/user/list-user/list-user.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlci9saXN0LXVzZXIvbGlzdC11c2VyLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/components/user/list-user/list-user.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/components/user/list-user/list-user.component.ts ***!
  \******************************************************************/
/*! exports provided: ListUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListUserComponent", function() { return ListUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/users.service */ "./src/app/services/users.service.ts");
/* harmony import */ var src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/generalservice.service */ "./src/app/services/generalservice.service.ts");




let ListUserComponent = class ListUserComponent {
    constructor(userservice, generalservice) {
        this.userservice = userservice;
        this.generalservice = generalservice;
    }
    ngOnInit() {
        this.list();
    }
    list() {
        this.userservice.list().subscribe(data => {
            this.listData = data;
        });
    }
    enableuser(usuario) {
        this.userservice.setactive(usuario).then(() => {
            this.generalservice.eventShowAlert.emit({ text: 'Usuario Inactivo', type: 'success' });
        });
    }
};
ListUserComponent.ctorParameters = () => [
    { type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"] },
    { type: src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_3__["GeneralserviceService"] }
];
ListUserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list-user',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./list-user.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/list-user/list-user.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./list-user.component.scss */ "./src/app/components/user/list-user/list-user.component.scss")).default]
    })
], ListUserComponent);



/***/ }),

/***/ "./src/app/components/user/register/register.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/components/user/register/register.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlci9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/components/user/register/register.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/user/register/register.component.ts ***!
  \****************************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_areaservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/areaservice.service */ "./src/app/services/areaservice.service.ts");
/* harmony import */ var src_app_services_users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/users.service */ "./src/app/services/users.service.ts");
/* harmony import */ var src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/generalservice.service */ "./src/app/services/generalservice.service.ts");
/* harmony import */ var src_app_services_pagos_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/pagos.service */ "./src/app/services/pagos.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");
/* harmony import */ var src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/authservice.service */ "./src/app/services/authservice.service.ts");









let RegisterComponent = class RegisterComponent {
    constructor(areasservice, usersservice, generalservice, pagosservice, route, afs, authservice) {
        this.areasservice = areasservice;
        this.usersservice = usersservice;
        this.generalservice = generalservice;
        this.pagosservice = pagosservice;
        this.route = route;
        this.afs = afs;
        this.authservice = authservice;
        this.thePassword = '';
        this.registerData = {
            id: '',
            email: '',
            firstName: '',
            lastName: '',
            identity: '',
            identityType: 'dni',
            active: true,
            type: 2,
            areas: []
        };
        this.registerPay = {
            id_user: '',
            ticket: '',
            comment: '',
        };
    }
    ngOnInit() {
        this.getAreas();
        if (this.route.snapshot.paramMap.get("id") !== null) {
            this.registerData.id = this.route.snapshot.paramMap.get("id");
            this.load();
        }
    }
    load() {
        this.usersservice.load(this.registerData.id).subscribe(data => {
            this.registerData = data.data();
            this.registerPay.id_user = this.registerData.id;
            this.pagos();
        });
    }
    store() {
        if (this.registerData.id) {
            this.usersservice.update(this.registerData).then(() => {
                this.generalservice.eventShowAlert.emit({ text: 'Registro exitoso', type: 'success' });
            }, error => {
                this.generalservice.eventShowAlert.emit({ text: 'Error en actualización de usuario', type: 'danger' });
            });
        }
        else {
            //console.log(this.registerData.type) 
            if (this.thePassword.length < 5 && (this.registerData.type === 1 || this.registerData.type === 3)) {
                this.generalservice.eventShowAlert.emit({ text: 'Error, falta definir correctamente la contrseña (debe ser mayor a 5 caracteres)', type: 'danger' });
            }
            else {
                const id = this.afs.createId();
                let usernew = this.registerData;
                usernew.id = id;
                this.usersservice.store(usernew).then(() => {
                    this.generalservice.eventShowAlert.emit({ text: 'Registro exitoso', type: 'success' });
                    this.registerData.id = usernew.id;
                    this.registerPay.id_user = usernew.id;
                    if ((this.registerData.type === 1 || this.registerData.type === 3)) {
                        this.authservice.Register(this.registerData.email, this.thePassword);
                    }
                }, error => {
                    //console.log(error)
                    this.generalservice.eventShowAlert.emit({ text: 'Error en creación de usuario', type: 'danger' });
                });
            }
        }
    }
    getAreas() {
        this.areasservice.list().subscribe(data => {
            this.areasData = data;
        });
    }
    storeArea(area) {
        this.registerData.areas.push({ id_area: area.id, name_area: area.name });
    }
    deleteArea(area) {
        let index = this.registerData.areas.indexOf(area);
        this.registerData.areas.splice(index, 1);
    }
    // PAGOS //
    pagos() {
        this.pagosservice.listPagos(this.registerData.id).subscribe(data => {
            this.listPays = data;
        });
    }
    addPay() {
        this.pagosservice.store(this.registerPay).then(data => {
            this.generalservice.eventShowAlert.emit({ text: 'Pago registrado', type: 'success' });
        }, error => {
            this.generalservice.eventShowAlert.emit({ text: 'Error en registro de pago', type: 'danger' });
        });
    }
    deletePay(pago) {
        this.pagosservice.delete(pago).then(() => {
            this.generalservice.eventShowAlert.emit({ text: 'Pago eliminado', type: 'success' });
        }, (error) => {
            this.generalservice.eventShowAlert.emit({ text: 'Error en eliminacion de pago', type: 'danger' });
        });
    }
};
RegisterComponent.ctorParameters = () => [
    { type: src_app_services_areaservice_service__WEBPACK_IMPORTED_MODULE_2__["AreaserviceService"] },
    { type: src_app_services_users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"] },
    { type: src_app_services_generalservice_service__WEBPACK_IMPORTED_MODULE_4__["GeneralserviceService"] },
    { type: src_app_services_pagos_service__WEBPACK_IMPORTED_MODULE_5__["PagosService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_7__["AngularFirestore"] },
    { type: src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_8__["AuthserviceService"] }
];
RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/register/register.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./register.component.scss */ "./src/app/components/user/register/register.component.scss")).default]
    })
], RegisterComponent);



/***/ }),

/***/ "./src/app/pipes/type-identity.pipe.ts":
/*!*********************************************!*\
  !*** ./src/app/pipes/type-identity.pipe.ts ***!
  \*********************************************/
/*! exports provided: TypeIdentityPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypeIdentityPipe", function() { return TypeIdentityPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TypeIdentityPipe = class TypeIdentityPipe {
    transform(value, ...args) {
        let type;
        switch (value) {
            case 'passport':
                type = 'Pasaporte';
                break;
            case 'dni':
                type = 'DNI';
                break;
            case 'ce':
                type = 'Carnet de Extranjeria';
                break;
            default: break;
        }
        return type;
    }
};
TypeIdentityPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'typeIdentity'
    })
], TypeIdentityPipe);



/***/ }),

/***/ "./src/app/pipes/type-user.pipe.ts":
/*!*****************************************!*\
  !*** ./src/app/pipes/type-user.pipe.ts ***!
  \*****************************************/
/*! exports provided: TypeUserPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypeUserPipe", function() { return TypeUserPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TypeUserPipe = class TypeUserPipe {
    transform(value, ...args) {
        let type;
        switch (value) {
            case 1:
                type = 'Administrador / Profesor';
                break;
            case 2:
                type = 'Estandar';
                break;
            case 3:
                type = 'Profesor';
                break;
            default: break;
        }
        return type;
    }
};
TypeUserPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'typeUser'
    })
], TypeUserPipe);



/***/ }),

/***/ "./src/app/services/areaservice.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/areaservice.service.ts ***!
  \*************************************************/
/*! exports provided: AreaserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AreaserviceService", function() { return AreaserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");
/* harmony import */ var _banks_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./banks.service */ "./src/app/services/banks.service.ts");




let AreaserviceService = class AreaserviceService {
    constructor(afs, banksservice) {
        this.afs = afs;
        this.banksservice = banksservice;
    }
    list() {
        let areasRef = this.afs.collection("areas");
        return areasRef.valueChanges();
    }
    get(areaId) {
        console.log(areaId);
        let areasRef = this.afs.collection("areas").doc(areaId);
        return areasRef.get();
    }
    store(data) {
        data.test.forEach(test => {
            if (test.id_bank === undefined || test.id_bank === '') {
                const id_bank = this.afs.createId();
                test.id_bank = id_bank;
                this.banksservice.store({ id: id_bank, questions: [] });
            }
            if (test.id === undefined || test.id === '') {
                test.id = this.afs.createId();
            }
        });
        const areasCollection = this.afs.collection("areas");
        const id = this.afs.createId();
        data.id = id;
        return areasCollection.doc(id).set(data);
    }
    update(data) {
        data.test.forEach(test => {
            if (test.id_bank === undefined || test.id_bank === '') {
                const id_bank = this.afs.createId();
                test.id_bank = id_bank;
                this.banksservice.store({ id: id_bank, questions: [] });
            }
            if (test.id === undefined || test.id === '') {
                test.id = this.afs.createId();
            }
        });
        return this.afs
            .collection("areas")
            .doc(data.id)
            .set(data);
    }
    delete(areaId) {
        let areasRef = this.afs.collection("areas").doc(areaId);
        return areasRef.delete();
    }
};
AreaserviceService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] },
    { type: _banks_service__WEBPACK_IMPORTED_MODULE_3__["BanksService"] }
];
AreaserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
    })
], AreaserviceService);



/***/ }),

/***/ "./src/app/services/authservice.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/authservice.service.ts ***!
  \*************************************************/
/*! exports provided: AuthserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthserviceService", function() { return AuthserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _generalservice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./generalservice.service */ "./src/app/services/generalservice.service.ts");






let AuthserviceService = class AuthserviceService {
    constructor(afs, // Inject Firestore service
    afAuth, // Inject Firebase auth service
    router, ngZone, generalservice) {
        this.afs = afs;
        this.afAuth = afAuth;
        this.router = router;
        this.ngZone = ngZone;
        this.generalservice = generalservice;
        this.afAuth.authState.subscribe(user => {
            if (user) {
                //console.log(JSON.parse(localStorage.getItem('user')))
                if (JSON.parse(localStorage.getItem('user')) === null || JSON.parse(localStorage.getItem('user')) === undefined) {
                    const userRef = this.afs.collection('usuarios', ref => ref.where('email', '==', user.email));
                    userRef.valueChanges().subscribe(dataExtra => {
                        this.userData = user;
                        localStorage.setItem('user', JSON.stringify(this.userData));
                        localStorage.setItem('userDataExtra', JSON.stringify(dataExtra));
                        this.router.navigate(['']);
                    });
                }
                //JSON.parse(localStorage.getItem('user'));
            }
            else {
                localStorage.setItem('user', null);
                localStorage.setItem('userDataExtra', null);
                //JSON.parse(localStorage.getItem('user'));
            }
        });
    }
    // Sign in with email/password
    LogIn(email, password) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password);
    }
    // Sign up with email/password
    Register(email, password) {
        return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
            .then((result) => {
            console.log(result);
            //this.SendVerificationMail(); 
        }).catch((error) => {
            this.generalservice.eventShowAlert.emit({
                text: "Error registrando usuario",
                type: "error"
            });
        });
    }
    // Send email verfificaiton when new user sign up
    SendVerificationMail() {
        return this.afAuth.auth.currentUser.sendEmailVerification()
            .then(() => {
            this.router.navigate(['verify-email-address']);
        });
    }
    // Reset Forggot password
    ForgotPassword(passwordResetEmail) {
        return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
            .then(() => {
            window.alert('Se ha enviado a su correo electronico un link para cambiar su contraseña');
        }).catch((error) => {
            window.alert(error);
        });
    }
    // Returns true when user is looged in and email is verified
    get isLoggedIn() {
        const user = JSON.parse(localStorage.getItem('user'));
        return (user !== null) ? true : false;
    }
    getUserData() {
        const user = JSON.parse(localStorage.getItem('userDataExtra'));
        //console.log(user)
        //const userRef: AngularFirestoreCollection = this.afs.collection('usuarios', ref => ref.where('id', '==', user.uid) )
        return user[0];
    }
    /* Setting up user data when sign in with username/password,
    sign up with username/password and sign in with social auth
    provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
    SetUserData(user) {
        const userRef = this.afs.doc(`usuarios/${user.uid}`);
        return userRef.set(user, {
            merge: true
        });
    }
    // Sign out 
    LogOut() {
        return this.afAuth.auth.signOut().then(() => {
            localStorage.removeItem('user');
            this.router.navigate(['login']);
        });
    }
    getUser(id_user) {
        const userRef = this.afs.collection('usuarios').doc(id_user);
        return userRef.get();
    }
    checkNroBoleta(nroBoleta) {
        const searchBoleta = this.afs.collection('pagos', ref => ref.where('ticket', '==', nroBoleta));
        return searchBoleta.get();
    }
};
AuthserviceService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _generalservice_service__WEBPACK_IMPORTED_MODULE_5__["GeneralserviceService"] }
];
AuthserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthserviceService);



/***/ }),

/***/ "./src/app/services/banks.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/banks.service.ts ***!
  \*******************************************/
/*! exports provided: BanksService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BanksService", function() { return BanksService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");



let BanksService = class BanksService {
    constructor(afs) {
        this.afs = afs;
    }
    store(data) {
        const areasCollection = this.afs.collection("bancos");
        return areasCollection.doc(data.id).set(data);
    }
    get(bankId) {
        let bankRef = this.afs.collection("bancos").doc(bankId);
        return bankRef.get();
    }
    update(data) {
        let bankRef = this.afs.collection("bancos").doc(data.id);
        return bankRef.set(data);
    }
    delete(id) {
        let bankRef = this.afs.collection("bancos").doc(id).delete();
    }
};
BanksService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
BanksService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], BanksService);



/***/ }),

/***/ "./src/app/services/generalservice.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/generalservice.service.ts ***!
  \****************************************************/
/*! exports provided: GeneralserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeneralserviceService", function() { return GeneralserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let GeneralserviceService = class GeneralserviceService {
    constructor(http) {
        this.http = http;
        this.eventShowAlert = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    getPicOfDay() {
        return this.http.get("https://cors-anywhere.herokuapp.com/https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=es-MX");
    }
};
GeneralserviceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
GeneralserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], GeneralserviceService);



/***/ }),

/***/ "./src/app/services/pagos.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/pagos.service.ts ***!
  \*******************************************/
/*! exports provided: PagosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagosService", function() { return PagosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");



let PagosService = class PagosService {
    constructor(afs) {
        this.afs = afs;
    }
    // cargar un pagos por cliente
    listPagos(id) {
        let pagos = this.afs.collection('pagos', ref => ref.where('id_user', '==', id));
        return pagos.valueChanges();
    }
    store(data) {
        const usersCollection = this.afs.collection('pagos');
        const id = this.afs.createId();
        data.id = id;
        return usersCollection.doc(id).set(data);
    }
    delete(data) {
        return this.afs.collection("pagos").doc(data.id).delete();
    }
};
PagosService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
PagosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], PagosService);



/***/ }),

/***/ "./src/app/services/pruebas.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/pruebas.service.ts ***!
  \*********************************************/
/*! exports provided: PruebasService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PruebasService", function() { return PruebasService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");



let PruebasService = class PruebasService {
    constructor(afs) {
        this.afs = afs;
        //this.headers = new HttpHeaders( { Authorization: localStorage.getItem('token')  } ) 
    }
    list() {
        let areasRef = this.afs.collection("test");
        return areasRef.valueChanges();
    }
    get(testId) {
        let areasRef = this.afs.collection("test").doc(testId);
        return areasRef.get();
    }
    getHistory(userId) {
        let areasRef = this.afs.collection("test", test => test.where('id_user', '==', userId));
        return areasRef.valueChanges();
    }
    getTestsPendingEvaluate() {
        let areasRef = this.afs.collection("test", test => test.where('status', '==', 1));
        return areasRef.valueChanges();
    }
    getTestsHistoryEvaluate() {
        let areasRef = this.afs.collection("test", test => test.where('status', '==', 2));
        return areasRef.valueChanges();
    }
    store(data) {
        const testCollection = this.afs.collection('test');
        return testCollection.doc(data.id).set(data);
    }
    update(data) {
        /* let areasRef: AngularFirestoreCollection = this.afs.collection('areas', ref => ref.where('id', '==', data.id) );
         areasRef.set(data, { merge: true })
         return areasRef.valueChanges()*/
    }
};
PruebasService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
PruebasService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], PruebasService);



/***/ }),

/***/ "./src/app/services/users.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/users.service.ts ***!
  \*******************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");



let UsersService = class UsersService {
    constructor(afs) {
        this.afs = afs;
    }
    list() {
        return this.afs.collection('usuarios').valueChanges();
    }
    listEstandar() {
        return this.afs.collection('usuarios', usuario => usuario.where('type', '==', 2)).valueChanges();
    }
    get(id) {
        let areasRef = this.afs.collection("usuarios").doc(id);
        return areasRef.get();
    }
    // cargar un registro
    load(id) {
        return this.afs.collection('usuarios').doc(id).get();
    }
    store(data) {
        const usersCollection = this.afs.collection('usuarios');
        return usersCollection.doc(data.id).set(data);
    }
    update(data) {
        return this.afs.collection('usuarios').doc(data.id).set(data);
    }
    setactive(data) {
        return this.afs.collection('usuarios').doc(data.id).update({
            active: false
        });
    }
};
UsersService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
UsersService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UsersService);



/***/ }),

/***/ "./src/app/shared/guard/auth.guard.ts":
/*!********************************************!*\
  !*** ./src/app/shared/guard/auth.guard.ts ***!
  \********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/authservice.service */ "./src/app/services/authservice.service.ts");




let AuthGuard = class AuthGuard {
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    canActivate(next, state) {
        if (this.authService.isLoggedIn !== true) {
            this.router.navigate(['login']);
        }
        return true;
    }
};
AuthGuard.ctorParameters = () => [
    { type: src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__["AuthserviceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthGuard);



/***/ }),

/***/ "./src/app/shared/guard/secure-inner-pages.guard.ts.guard.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shared/guard/secure-inner-pages.guard.ts.guard.ts ***!
  \*******************************************************************/
/*! exports provided: SecureInnerPagesGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecureInnerPagesGuard", function() { return SecureInnerPagesGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/authservice.service */ "./src/app/services/authservice.service.ts");




let SecureInnerPagesGuard = class SecureInnerPagesGuard {
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    canActivate(next, state) {
        if (this.authService.isLoggedIn) {
            window.alert("Sin acceso a esta url");
            this.router.navigate(['']);
        }
        return true;
    }
};
SecureInnerPagesGuard.ctorParameters = () => [
    { type: src_app_services_authservice_service__WEBPACK_IMPORTED_MODULE_3__["AuthserviceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
SecureInnerPagesGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], SecureInnerPagesGuard);



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

const environment = {
    production: true,
    name_company: 'ESCONFA INTERNACIONAL',
    firebase: {
        apiKey: "AIzaSyAGIfIQi2hVfj4UzivazDj94ZocK_CGMmM",
        authDomain: "plataforma-evaluacion.firebaseapp.com",
        databaseURL: "https://plataforma-evaluacion.firebaseio.com",
        projectId: "plataforma-evaluacion",
        storageBucket: "plataforma-evaluacion.appspot.com",
        messagingSenderId: "885046006968",
        appId: "1:885046006968:web:159780ee2f2f21b28e504d",
        measurementId: "G-83SHK0WFCN"
    }
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false,
    name_company: 'ESCONFA INTERNACIONAL',
    firebase: {
        apiKey: "AIzaSyDlE2Du6Y8OVE4hz8Mk-oNjyJklDugBEhY",
        authDomain: "plataforma-cesia-f9c41.firebaseapp.com",
        databaseURL: "https://plataforma-cesia-f9c41.firebaseio.com",
        projectId: "plataforma-cesia-f9c41",
        storageBucket: "plataforma-cesia-f9c41.appspot.com",
        messagingSenderId: "388478824692",
        appId: "1:388478824692:web:e85632e1a55dbbbcda6ea1"
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\eBiolibros\Documents\plataforma-cesia\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map